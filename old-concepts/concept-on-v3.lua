package.path = package.path .. ";C:\\Work\\lua"

require "var_dump"

local d = var_dump
local on = {}

local _data = {
	id = "",
	state = "RELEASED",
	press = function(...)
		d("_data.press")
		d(...)
		-- Set this key as PRESSED
	end,
	release = function()
		d("_data.release")
		-- Set this key as RELEASED
	end
}

local _meta = {
	__newindex = function(self, key, data)
		d("__newindex", self, key, data)

		rawset(self, key, data)
	end,
	__index = function(self, key)
		if rawget(self, key) ~= nil then
			return self[key] end

			local _parent = self
			local _keyData = {
				id = key,
				state = "RELEASED",
				bind = {
					press = function(self, fn)
						d("_keyData.bind.press()", self, fn)
					end,
					release = function(self, fn)
						d("_keyData.bind.release()", self, fn)
					end
				},
				press = function(self)
					d("_keyData.press()")
				end,
				release = function(self)
					d("_keyData.release()")
				end
			}

			local _k = {
				id = key,
				state = "RELEASED",
				bind = {},
				press = function(self)
					d("_keyData.press()")
				end,
				release = function(self)
					d("_keyData.release()")
				end
			}

			_k.bind.press = function(self, fn)
				d("_keyData.bind.press()", self, fn)
				d("_k", _k)
			end

			_k.bind.release = function(self, fn)
				d("_keyData.bind.release()", self, fn, _k)
			end

			rawset(self, key, _k)

			return self[key]

			local _keyMeta = {
				__newindex = function(self, key, value)
					d("_keyMeta.__newindex", self, key, value)

					local _fn = function()
						self.state = "PRESSED"

						value(_parent, self)
					end

					rawset(self, key, _fn)
				end,
				__index = function(self, key)
					d("_keyMeta.__index()", self, key)
				end
			}

			setmetatable(_keyData, _keyMeta)

			if rawget(self, key) == nil then
				rawset(self, key, _keyData) end

				return self[key]
			end
		}

		setmetatable(on, _meta)

		--d("ON INIT", on)
		-- d("ON CREATE G22", on.G22)
		-- d("ON POST G22", on)

		on.G22.bind.press(function(on, key)
			d("attempting to force release key")

			-- key.release()
		end)

		on.G22.press()
		on.G22.release()

		local ref = "G22"
		local run = "press"

		-- d(on[ref][run]())
