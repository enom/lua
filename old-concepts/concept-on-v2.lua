require "var_dump"

local d = var_dump
local p = print

-- Lookup table holding all of our bindings
local on = {has = {}}
local _m = {
	__index = function(self, key)
		p("_meta.__index("..key..")")

		if rawget(self, key) ~= nil then
			return self[key] end

			self[key] = {
				key = key,
				state = "RELEASED",
				-- Dynamically bind a press or release function
				bind = function(case, callback)
					p(key.." bind()")

					-- Reference to our own object
					local this = self[key]

					-- Release allows us to trigger release after the press function
					-- executes
					this[case] = function(release)
						p(key.." internal "..case.." rigging")

						if case == "press" then
							this.state = "PRESSED" end

							if case == "release" then
								this.state = "RELEASED" end

								callback(this)

								if release == true then
									this.release() end
								end
							end,
							-- Default press function which also allows automatic release
							-- Updated the current key state
							press = function(release)
								p(key.." default press()")
								d(release)

								self[key].state = "PRESSED"

								if release == true then
									self[key].release() end
								end,
								-- Default release function just updates the key state
								release = function()
									p(key.." default release()")

									self[key].state = "RELEASED"
								end
							}

							-- Update our 'has' table for quick reference to see if we
							-- used bind()
							self.has[key] = true

							return self[key]
						end,
					}

					setmetatable(on, _m)

					--[[
G.on.press(22, function() p("PRESSING G22") end)
G.press(22)
--]]

					on.G22.bind("press", function(key) p("G22 executing press and release") end)
					on.G22.press(true)

					d(on.has.G22)

					p("Final table")
					d(on)
