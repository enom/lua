require "var_dump"

local d = var_dump
local p = print

local on = {}
local _m = {
	__newindex = function(self, key, value)
		if rawget(self, key) ~= nil then
			return self[key] end

			self[key] = value
		end,
		__index = function(self, key)
			d("__index", self, key)

			if rawget(self, key) ~= nil then
				return self[key] end

				local _data = {
					key = key,
					state = "RELEASED",
					press = function(self, fn)
						p("_data.press()")

						if fn == nil then
							p(key .. " trigger press()")

							return self.press()
						end

						self.press = function(self)
							p(key .. " bind press()")

							self.state = "PRESSED"

							fn(self)
						end
					end,
					release = function(self, fn)
						if fn == nil then
							p(key .. " trigger release()")

							return self.release()
						end

						self.release = function(self)
							p(key .. " binding release()")

							self.state = "RELEASED"

							fn(self)
						end
					end
				}

				-- GKey doesn't exist so create it
				rawset(self, key, _data)

				return self[key]
			end
		}

		setmetatable(on, _m)

		on.G22:release()


		--[[
G.press(22, function(key) d("G.press(22)") end)
G.bind.press(22, function(key) d("bind") end)
--]]
