--[[
--
--]]
function GFakeCall(...)
	local name = debug.getinfo(1, "n").name
	local str = ""

	for i, v in ipairs({...}) do
		str = (str == "" and "" or str..", ")..v end

		print(name.."("..str..")")
	end

	local clock = os.clock

	function sleep(s) -- seconds
		local n = s / 1000
		local t0 = clock()
		while clock() - t0 <= n do end
	end

	OnEvent = GFakeCall
	GetMKeyState = GFakeCall
	SetMKeyState = GFakeCall
	Sleep = function(n) sleep(n) end
	OutputLogMessage = GFakeCall
	GetRunningTime = GFakeCall
	GetDate = GFakeCall
	ClearLog = GFakeCall
	PressKey = GFakeCall
	ReleaseKey = GFakeCall
	PressAndReleaseKey = GFakeCall
	IsModifierPressed = GFakeCall
	PressMouseButton = GFakeCall
	ReleaseMouseButton = GFakeCall
	PressAndReleaseMouseButton = GFakeCall
	IsMouseButtonPressed = GFakeCall
	MoveMouseTo = GFakeCall
	MoveMouseWheel = GFakeCall
	MoveMouseRelative = GFakeCall
	MoveMouseToVirtual = GFakeCall
	GetMousePosition = function() return 20000, 40000 end
	OutputLCDMessage = GFakeCall
	ClearLCD = GFakeCall
	PlayMacro = GFakeCall
	AbortMacro = GFakeCall
	IsKeyLockOn = GFakeCall
	SetBacklightColor = GFakeCall
	OutputDebugMessage = GFakeCall
	SetMouseDPITable = GFakeCall
	SetMouseDPITableIndex = GFakeCall
	EnablePrimaryMouseButtonEvents = GFakeCall
