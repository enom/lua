require "var_dump"
require "sleep"

local v = var_dump
local p = function(...) print(string.format(unpack({...}))) end

--------------------------------------------------------------------------------

local Wait = function()
	local _api = {}
	local _inside = {}
	local _outside = {}

	local _resolve = function() end
	local _reject = function() end

	_api.prepare = function(resolve, reject)
		_resolve = resolve or _resolve
		_reject = reject or _reject

		return _outside
	end

	_outside.run = function(callback)
		callback(_inside)
	end

	_inside.resolve = function(...)
		_resolve(...)
	end

	_inside.reject = function(...)
		_reject(...)
	end

	return _api
end

--------------------------------------------------------------------------------

local waitPrepare = Wait().prepare(function(...)
	p("waitPrepare().prepare(resolve)")
	v(...)
	end, function(...)
		p("waitPrepare().prepare(reject)")
		v(...)
		end).run(function(self)
			p("waitPrepare().run()")

			sleep(1000)

			self.resolve("resolved")
			self.reject("rejected")
		end)

		--

		local waitTwo = Wait().prepare(function()
			p("waitTwo().prepare(resolve)")
			end).run(function(self)
				p("waitTwo().run() -- immediate")

				self.resolve()
			end)
