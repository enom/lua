require "var_dump"

local p = function(...) print(string.format(unpack({...}))) end
local d = var_dump

--[[
{
    super = {
        {fn1},
        {fn2},
        child = {
            {fnc1}
        }
    }
}
on(super.*)
emit(super.child)

select super
insert * key
insert functions in *
read all children using pairs()
for each child function using #child
--]]

--

--

local event = (function()
	local _e = {}
	local _o = {}
	local api = {}

	-- Private functions
	function _getEventsByNamespace(namespace, build, reference)
		-- p("\n_getEventsByNamespace")
		-- p("namespace")
		-- d(namespace)

		-- p("\nbuild")
		-- d(build)

		-- p("\nreference")
		-- d(reference ~= nil)
		local stack = {}
		local build = build or false
		local space = {}
		-- Target array to fetch reference
		local deep = reference or _e

		-- Get depth for namespace assigning
		for name in string.gmatch(namespace, "([^.]+)") do
			-- d("name "..name)
			table.insert(space, name)
		end

		-- Recurse into our events with namespace
		for i = 1, #space do
			local ns = space[i]
			-- d(ns, deep)

			-- We're not creating our events as we recurse into our namespace
			if not deep[ns] and not build then
				return {}
			end

			-- Create an array if one doesn't exist to keep recursing
			if not deep[ns] then
				deep[ns] = {}
			end

			-- Add functions to call stack
			for k, v in pairs(deep[ns]) do
				p("_getEvents namespace = "..ns..", k = "..k)
				d(v)
			end

			-- Update our depth reference
			deep = deep[ns]
		end

		return deep
	end

	-- Public functions / API
	api.on = function(event, handler)
		-- Fetches a deep reference to our events array and creates it if needed
		local ns = _getEventsByNamespace(event, true)

		-- Inserting the handler here will update our global events since it's a reference
		table.insert(ns, {callback = handler, once = false})
	end

	api.one = function(event, handler)
		-- Fetches a deep reference to our events array
		local ns = _getEventsByNamespace(event, true)

		-- Specifies that this event should only fire once
		table.insert(ns, {callback = handler, once = true})
	end

	api.off = function(event)
		-- Trim removes lingering dots
		local trim = function(str)
			return string.gsub(string.gsub(str, "^[.]+", ""), "[.]+$", "")
		end

		-- Clean up namespace references
		local e = trim(event)
		local parent = string.gsub(e, ".%a+$", "")
		local child = trim(string.gsub(e, parent, ""))

		if parent == "" then
			-- We're removing all events straight from our global
			_e = {}
		else
			-- Get namespace to remove events from
			local ns = _getEventsByNamespace(parent)

			-- Remove all sub events from the namespace
			ns[child] = nil
		end
	end

	api.emit = function(event, ...)
		local ns = _getEventsByNamespace(event)

		-- p("emit "..event)
		-- d(ns)

		-- Execute all functions bound to this namespace
		for key, data in pairs(ns) do
			if type(key) == "string" then
				-- String keys are addition namespace depth we need to
				-- traverse with the provided arguments
				api.emit(event.."."..key, ...)
			else
				-- Numerical keys are functions to execute
				data.callback(...)

				-- Remove function if it should only be called once
				if data.once then
					ns[key] = nil
				end
			end
		end
	end

	api.debug = function()
		p("\n-------------------------------\nBEGIN DEBUG\n")

		d(_e)

		p("\nEND DEBUG\n-------------------------------\n")
	end

	api.recurse = function(namespace)
		p("recurse "..namespace)
	end

	return api
end)()

--[[
-- Bind
p("\nBINDING\n-------------------------------")
event.on("super.child", function()
    p("GOT super.child")
end)

event.on("super", function()
    p("GOT super")
end)

event.on("super.child.deep", function()
    p("GOT super.child.deep")
end)

event.on("super.child", function()
    p("GOT super.child TWO")
end)

event.one("emit.once", function()
    p("GOT emit.once")
end)

event.on("emit.off", function()
    p("GOT emit.off")
end)
--]]

--[[
-- Call
p("\nEMITING\n-------------------------------")
p("Following should print super.child and super.child.deep")
event.emit("super.child")

p("\nFollowing should print super, super.child and super.child.deep")
event.emit("super")

p("\nFollowing should print super.child.deep")
event.emit("super.child.deep")
--]]


--[[
-- Single
p("\nONCE OFF\n-------------------------------")

p("\nFollowing should print emit.off only ONCE")
event.emit("emit.off")
event.off("emit.off")
event.emit("emit.off")

p("\nFollowing should print emit.once only ONCE")
event.emit("emit.once")
event.emit("emit.once")
event.debug()

p("\nFollowing should print NOTHING!")
event.off("super")
event.emit("super.child")
event.emit("super")
event.emit("super.child.deep")

event.on("emit.once", function()
    p("Turning emit.once back on with new callback")
end)

event.emit("emit")
event.debug()
--]]

-- [[
-- Listen to all child namespaces
-- event.on("parent", function() p("Got parent") end)
-- event.on("parent.first", function() p("Got parent.first") end)
-- event.on("parent.second", function() p("Got parent.second") end)
--event.on("parent", function() p("Got parent") end)
--event.on("parent.second", function() p("Got parent.second") end)
--
--p("\nemit parent should fire all")
--event.emit("parent")
--
--p("\nemit parent.first should fire parent and parent.first")
--event.emit("parent.first")
--
--p("\nemit parent.second should fire parent and parent.second")
--event.emit("parent.second")
--]]
event.on("super", function() p("\nON SUPER\nShould not fire") end)
event.on("super.child", function() p("\nON SUPER.CHILD\nShould fire") end)
event.on("super.*", function() p("\nON SUPER.*\nShould fire") end)
event.recurse("super.child")
