require "g_functions"
require "var_dump"

local p = function(...) print(string.format(unpack({...}))) end
local d = var_dump

Q = function(ms)
	local _ms = ms or 10

	local _mouse = {}
	local _mouseHeld = {}
	local _mouseCapture = {}

	local _key = {}
	local _keyHeld = {}

	-- Key functions
	_key.press = function(...)
		p("\nQ().k.press()")

		PressAndReleaseKey(...)
		Sleep(_ms)

		return _key
	end

	_key.hold = function(...)
		p("\nQ().k.hold()")

		for k, v in ipairs({...}) do
			table.insert(_keyHeld, v) end

			PressKey(...)
			Sleep(_ms)

			return _key
		end

		_key.release = function(...)
			-- p("\nQ().k.release()")

			if #_keyHeld == 0 then
				return _key end

				local args = {...}

				if #args == 0 then
					p("\nQ().k.release() previous")

					-- Release the last key being held
					ReleaseKey(table.remove(_keyHeld))
				elseif args[1] == true then
					p("\nQ().k.release() all")

					-- Release all keys being held
					ReleaseKey(unpack(_keyHeld))

					_keyHeld = {}
				else
					p("\nQ().k.release() specific")
					local keys = {}

					for i = 1, #args do
						-- Look through arguments provided
						for k, v in ipairs(_keyHeld) do
							-- Loop through all keys held to remove the ones specified
							if v == args[i] then
								table.insert(keys, table.remove(_keyHeld, k)) end
							end
						end

						-- Do mass unsetting via Logitech's API
						ReleaseKey(unpack(keys))
					end

					Sleep(_ms)

					return _key
				end

				_key.run = function(...)
					p("\nQ().k.run()")

					local args = {...}

					for i = 1, #args do
						args[i](_key) end

						return _key
					end

					_key.sleep = function(ms)
						p("\nQ().k.sleep(%d)", ms)

						Sleep(ms)

						return _key
					end

					-- Mouse functions
					_mouse.press = function(btn)
						p("\nQ().m.press(%i)", btn)

						PressAndReleaseMouseButton(btn)
						Sleep(_ms)

						return _mouse
					end

					_mouse.hold = function(btn)
						p("\nQ().m.hold(%i)", btn)

						table.insert(_mouseHeld, btn)

						PressMouseButton(btn)
						Sleep(_ms)

						return _mouse
					end

					_mouse.release = function(btn)
						-- p("\nQ().m.release()")
						-- No keys held to release
						if #_mouseHeld == 0 then
							return _mouse end

							if btn == nil then
								-- Release previous key pressed
								p("\nQ().m.release() previous ".._mouseHeld[#_mouseHeld])

								-- Does an array.pop and returns the keycode to remove
								ReleaseMouseButton(table.remove(_mouseHeld))
							elseif btn == true then
								-- Release all keys pressed
								p("\nQ().m.release() all")

								for i = #_mouseHeld, 1, - 1 do
									-- Traverse held keys backwards and release them
									ReleaseMouseButton(_mouseHeld[i]) end

									-- Reset our array
									_mouseHeld = {}
								elseif _mouseHeld[btn] ~= nil then
									-- Release one key pressed
									p("\nQ().m.release() key %s", btn)

									for i, v in ipairs(_mouseHeld) do
										-- Find the right key index to remove from our array and release that key
										if v == btn then
											ReleaseMouseButton(table.remove(_mouseHeld, i)) end
										end
									end

									Sleep(_ms)

									return _mouse
								end

								_mouse.capture = function(id)
									p("\nQ().m.capture(%s) x = %d, y = %d", id, GetMousePosition())

									local x, y = 100, 200

									_mouseCapture[id] = {GetMousePosition()}

									return _mouse
								end

								_mouse.move = function(x, y, relative)
									-- p("\nQ().m.move()")

									local c = _mouseCapture

									if type(x) == "string" and c[x] ~= nil then
										-- Move to a saved capture
										p("\nQ().m.move() to capture %s", x)

										MoveMouseTo(c[x][1], c[x][2])
									elseif relative == true then
										-- Move relative to current cursor position
										p("\nQ().m.move() relative %d, %d", x, y)

										local _x, _y = GetMousePosition()
										MoveMouseTo(_x + x, _y + y)
									else
										-- Move to absolute position
										p("\nQ().m.move() absolute")

										MoveMouseTo(x, y)
									end

									Sleep(_ms)

									return _mouse
								end

								_mouse.run = function(...)
									p("\nQ().m.run()")

									local args = {...}

									for i = 1, #args do
										args[i](_mouse) end

										return _mouse
									end

									_mouse.sleep = function(ms)
										p("\nQ().m.sleep() %d", ms)

										Sleep(ms)

										return _mouse
									end

									_mouse.wait = function(button)

										Sleep(500)

										return _mouse
									end

									-- Add ability to jump between the two
									_key.m = _mouse
									_mouse.k = _key

									return {
										k = _key,
										m = _mouse
									}
								end

								--[[
Q()
	.k.press("a", "b")
	  .hold("alt")
	.m.capture("origin")
	  .hold(1)
	  .move(0, 1000, true)
	  .release()
	.k.release()
	.m.move("origin")
--]]

								Q()
								.m.wait(1)
								.press(1)
