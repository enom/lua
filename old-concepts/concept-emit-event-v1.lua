require "var_dump"

local d = var_dump

local event = (function()
	local _listeners = {}
	local _one = {}
	local _api = {}

	_api.emit = function(id, ...)
		if _listeners[id] == nil then
			return end

			for i = 1, #_listeners[id] do
				_listeners[id][i](...) end

				if _one[id] == true then
					_api.off(id) end
				end

				_api.on = function(id, callback)
					if type(callback) ~= "function" then
						error("event.on() requires second parameter to be a function") end

						if _listeners[id] == nil then
							_listeners[id] = {} end

							table.insert(_listeners[id], callback)
						end

						_api.one = function(id, callback)
							_one[id] = true
							_api.on(id, callback)
						end

						_api.off = function(id)
							_listeners[id] = nil
							_one[id] = nil
						end

						return _api
					end)()

					-- ON --
					print("\nON\n")
					event.on("on", function()
						print("got on one")
					end)

					event.on("on", function()
						print("got on two")
					end)

					event.emit("on")

					-- CALLBACK --
					print("\nCALLBACK\n")
					event.on("callback", function(callback)
						print("on callback")
						callback(1, 2)
					end)

					event.emit("callback", function(a, b)
						print("emit callback a = "..a..", b = "..b)
					end)

					-- ONE --
					print("\nONE\n")
					event.one("one", function()
						print("one should only execute once")
					end)

					event.emit("one")
					event.emit("one")

					-- OFF --
					print("\nOFF\n")
					event.on("off", function()
						print("off should only fire once")
					end)

					event.emit("off")
					event.off("off")
					event.emit("off")
