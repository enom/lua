local clock = os.clock

function sleep(s) -- seconds
	local n = s / 1000 -- milliseconds
	local t0 = clock()

	while clock() - t0 <= n do end
end
