require "var_dump"

local d = var_dump
local p = function(...)
	print(string.format(unpack({...})))
end

--[[
-- G2 PRESSED - Danger Ping
--
on.G2.bind("press", function(key, on)
	Q({

	})
end)
-- End G2 PRESSED ]]

M = {
	queue = function(button, queue)
		p("M.queue(%s)", button)
		d(queue)

		return M
	end,
	nudge = function(x, y)
		p("M.nudge(%d, %d)", x, y)

		return M
	end
}

Q = {
	queue = function(key, chain)
		p("Q.queue(%s)", key)
		d(chain)

		for i = 1, table.maxn(chain) do
			p("got queue argument")
		end
	end
}

--[[
F = {
	test = function()
		p("F.test()")

		return "F.return"
	end
}

T = {
	"one",
	"two",
	"three",
	F.test()
}

for i = 1, table.maxn(T) do
	d(i, T[i]) end
--]]

-- Hold alt
Q.queue("alt", {
	-- Hold mouse button 1
	M.queue(1, {
		-- Move mouse down to danger ping
		M.nudge(0, - 1000)
	}) -- Release mouse 1
}) -- Release alt
