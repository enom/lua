function chain(cmd)
	--Loop through each argument
	for i = 1, table.maxn(arg) do
		--If argument is a table
		if (type(arg[i]) == "table") then
			--Send argument table back to "key"
			call("chain", arg[i]);
		else
			--Argument isn't table
			if (abort ~= true) then
				a = explode(", ", arg[i]);

				if (a[1] == "move" ) then
					print("move");
					MoveMouseTo(a[2], a[3])
				elseif (a[1] == "mouse") then

					PressMouseButton(a[2])
				elseif (a[1] == "key") then
					PressKey(a[2])
				elseif (a[1] == "sleep") then
					call(a[1], tonumber(a[2]));
				end --//switch case

				call("sleep", 200)
			end --//if ~abort
		end --//if table
	end --//for arguments

	--Loop to release keys
	for i = table.maxn(arg), 1, - 1 do
		--If argument isn't table
		if (type(arg[i]) ~= "table") then
			if (abort ~= true) then
				a = explode(", ", arg[i]);

				if (a[1] == "mouse") then
					ReleaseMouseButton(a[2])
				elseif (a[1] == "key") then
					ReleaseKey(a[2])
				end --//switch case

				call("sleep", 200)
			end --//if ~abort
		end
	end --//for release
end -- chain

--[[
 - Interruptible sleep
 -
 - @param t int
 -]]
function doSleep(t)

end -- sleep
