-- require g-key-vars@1.0.1
-- require g-key-loop@3.2.0

----------------------------
-- MOUSE FUNCTIONS v3.5.0 --
----------------------------

local M = {
	-- move mouse cursor
	m = function(x, y)
		if type(x) == "table" then x, y = unpack(x) end

		MoveMouseRelative(x, y)
	end,
	-- press mouse button
	p = function(btn)
		if L.stop() then return end

		PressMouseButton(btn)
		Sleep(MS)
	end,
	-- get/set mouse position
	pos = function(x, y)
		if x == nil then return GetMousePosition() end
		if type(x) == "table" then x, y = unpack(x) end

		MoveMouseTo(x, y)
	end,
	-- press, hold, and release mouse button
	pr = function(btn, hold)
		if L.stop() then return end

		PressMouseButton(btn)

		if hold == nil then Sleep(MS) else L.wait(hold) end

		ReleaseMouseButton(btn)
		Sleep(MS)
	end,
	-- release mouse button
	r = function(btn)
		ReleaseMouseButton(btn)
		Sleep(MS)
	end
}
