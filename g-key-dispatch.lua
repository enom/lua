-- require g-key-bind@1.0.0

-----------------------
-- DISPATCHER v3.5.0 --
-----------------------

function OnEvent(event, arg)
	local g = B["M" .. B.M] and B["M" .. B.M]["G" .. arg]
	local mouse = event == "MOUSE_BUTTON_PRESSED"
		or event == "MOUSE_BUTTON_RELEASED"
	local callback = event == "G_PRESSED" and "press"
		or event == "MOUSE_BUTTON_PRESSED" and "press"
		or event == "G_RELEASED" and "release"
		or event == "MOUSE_BUTTON_RELEASED" and "release"

	if event == "M_RELEASED" then B.M = arg end
	if event == "PROFILE_DEACTIVATED" then B:reset() end
	if g and g[callback] then g[callback](g, mouse) end
end
