-- require g-key-gamepad@1.1.0
-- require g-key-loop@3.2.0

---------------------------
-- STEP FUNCTIONS v1.1.1 --
---------------------------

local S = {
	-- tracks what steps were chosen
	path = {},
	-- flag enabling the capturing of steps
	run = false,
	-- current step
	step = nil,
	-- steps to be configured
	steps = {},
	-- configures the step table
	configure = function(self, config)
		for key, value in pairs(config) do
			self[key] = value
		end
	end,
	-- function called when next reaches a value
	done = function(self, value) end,
	-- handles capturing events and stepping
	next = function(self, index)
		local next = self.run

		if next then
			table.insert(self.path, index)

			local step = (self.step or self.steps)[index]

				if type(step) == "table" then
				self.step = step
				self:options()
			elseif step ~= nil then
				self:done(step)
				self:reset()
			end
		end

		return next
	end,
	-- lists the available steps on the gamepad
	options = function(self)
		local display = ""
		local steps = self.step or self.steps

		for i = 1, 3 do
			local ii = i + 3
			local left = steps[i] or ""
			local right = ii == 6 and "cancel" or steps[ii] or ""

			if type(left) == "table" then
				left = left.label or #left .. " options"
			end

			if type(right) == "table" then
				right = right.label or #right .. " options"
			end

			if #left > 12 then left = left:sub(1, 10) .. "..." end
			if #right > 12 then right = right:sub(1, 10) .. "..." end

			display = display .. "G" .. i .. " " .. left .. "\t"
			display = display .. "G" .. ii .. " " .. right .. "\n"
		end

		G.clear(false)
		G:teal(display)
	end,
	-- reset the steps and disable capture
	reset = function(self, ms, reason)
		self.run = false
		self.step = nil

		if type(ms) == "number" then L.wait(ms, reason, false) end

		G.clear()
	end,
	-- enable/disable event capture for steps
	toggle = function(self)
		if self.run then
			self:reset(1000, "cancelled")
		else
			self.run = true
			self.path = {}
			self:options()
		end
	end
}
