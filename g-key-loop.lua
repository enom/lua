-- require g-key-vars@1.0.1
-- require g-key-gamepad@1.1.0

---------------------------
-- LOOP FUNCTIONS v3.2.0 --
---------------------------

local L = {
	-- internal loop counter
	i = -1,
	-- oscillate value flipped
	f = false,
	-- oscillate boolean value
	o = false,
	-- resets the loop counters
	new = function(self)
		self.i = -1
		self.f = false
		self.o = false
	end,
	-- increments a value while looping
	inc = function(self)
		self.i = self.i + 1

		return self.loop()
	end,
	-- increments and oscillates a value while looping
	osc = function(self, n)
		self.i = self.i + 1
		self.f = self.i % n == 0

		if self.f then self.o = not self.o end

		return self.loop()
	end,
	-- break out of loops using SHIFT
	loop = function()
		return not IsModifierPressed("lshift")
	end,
	-- break out of sleeps using SHIFT
	raw = function(ms)
		local max = time() + ms

		while time() < max do
			if IsModifierPressed("lshift") then break end

			Sleep(MS)
		end
	end,
	-- stop press events using SHIFT
	stop = function()
		return IsModifierPressed("lshift")
	end,
	-- PRETTY break out of sleeps using SHIFT
	wait = function(ms, reason, units)
		local display = (reason ~= false and reason ~= nil) and reason or "wait"
		local max = time() + ms
		local tail

		if reason ~= false and units ~= false then
			local number = ms < 1000 and ms
				or ms < 1000 * 60 and math.floor(ms / 10) / 100
				or math.floor(ms / (10 * 60)) / 100
			local unit = ms < 1000 and "ms"
				or ms < 1000 * 60 and "second"
				or "minute"

			if unit ~= "ms" and number ~= 1 then unit = unit .. "s" end

			tail = " " .. number .. " " .. unit
		end

		G:purple(G.center(display .. (tail or "")), ms)

		while time() < max do
			if IsModifierPressed("lshift") then break end

			Sleep(MS)
		end

		G.clear()
	end
}
