
---------------------
-- BINDINGS v1.0.0 --
---------------------

B:configure({
	M1 = {
		G9 = {
			release = function()
				B.macro({ 1, "~~" })
			end
		},
		G10 = {
			release = function()
				B.macro({ 1, "#" })
			end
		},
		G11 = {
			release = function()
				B.macro({ "&nbsp;" })
			end
		},
		G12 = {
			release = function()
				B.macro({ 1, "*" })
			end
		},
		G13 = {
			release = function()
				B.macro({ 1, "**" })
			end
		},
        G14 = {}, -- split PHPStorm horizontally
		G15 = {
			release = function()
				B.macro({ 1, {"enter"} })
			end
		},
		G16 = {
			release = function()
				B.macro({ 1, {"enter"}, "! " })
			end
		},
        G17 = {}, -- split PHPStorm vertically
		G18 = {
			release = function()
				B.macro({ 1, {"delete"}, {"spacebar"} })
			end
		},
		G19 = {
			release = function()
				B.macro({ 
					1,
					{"delete"},
					{"rshift", true},
					{"lctrl", true},
					{"right"},
					{"left"},
					{"lctrl", false},
					{"rshift", false},
					" "	
				})
			end
		},
		G20 = {
			release = function()
				B.macro({ {"minus"} })
			end
		}
	}
})

