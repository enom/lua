-- require g-key-keyboard@3.3.0
-- require g-key-loop@3.2.0
-- require g-key-mouse@3.5.0

---------------------------
-- BIND FUNCTIONS v1.2.0 --
---------------------------

local B = {
	-- current select M-key
	M = GetMKeyState(),
	-- configures the bind table
	configure = function(self, config)
		for key, value in pairs(config) do
			self[key] = value
		end
	end,
	-- trigger one or more G-key events
	emit = function(keys)
		if keys.press or keys.release then
			keys = {keys}
		end

		for i, key in ipairs(keys) do
			if L.stop() then return end

			if key.press then
				key.press(key)
			end

			if key.release then
				key.release(key)
			end

			L.wait(50)
		end
	end,
	-- simplifies calling multiple macros
	--  - table {integer, integer} = M.pos(x, y)
	--  - table {string, integer?} = K.pr(key, hold?)
	--  - table {string, boolean} = K.p(key) or K.r(key)
	--  - integer < 10 = M.pr()
	--  - integer >= 10 = L.wait()
	--  - string = K.type()
	macro = function(inputs, delay)
		for key, value in pairs(inputs) do
			if type(value) == "table" then
				local k, h = unpack(value)

				if type(k) == "string" then 
					if h == nil or type(h) == "number" then
						K.pr(k, h)
					else
						if h then K.p(k) else K.r(k) end
					end
				else M.pos(value) end
			elseif type(value) == "number" then
				if value < 10 then M.pr(value)
				else L.wait(value) end
			elseif type(value) == "string" then
				K.type(value)
			else
				local k, v = tostring(key), tostring(value)

				print("unknown key = " .. k .. ", value = " .. v .. "\n")
			end

			if type(delay) == "number" then L.raw(delay) end
		end
	end,
	-- called when profile is deactivated
	reset = function() end
}
