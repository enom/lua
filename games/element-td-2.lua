-- DEBUG --

function dump(o, d)
	local t, i = "{\n", "\t"
	if type(o) == "table" then
		local s = t
		for k, v in pairs(o) do
			if s ~= t then s = s .. ",\n" end
			s = s .. i:rep(d or 1) .. "[" .. k .. "] = " .. dump(v, (d or 1) + 1)
		end
		return s .. "\n" .. i:rep((d or 1) - 1) .. "}"
	elseif type(o) == "string" then
		return "'" .. o .. "'"
	else
		return tostring(o)
	end
end

-----------------------------
-- GLOBAL VARIABLES v1.0.1 --
-----------------------------

-- milliseconds for loops and holds
local MS = 10

-- alias certain functions
local print = OutputLogMessage
local time = GetRunningTime

------------------------------
-- GAMEPAD FUNCTIONS v1.1.0 --
------------------------------

local G = {
	-- changes gamepad background color
	bg = function(r, g, b)
		if type(r) == "table" then r, g, b = unpack(r) end

		SetBacklightColor(r, g, b, "lhc")
	end,
	-- properly spaces a message to center it on the gamepad
	center = function(message)
		return "\n" .. string.rep(" ", (50 - (#message * 1.618)) / 2) .. message .. "\n"
	end,
	-- clear gamepad color and text
	clear = function(color)
		if color ~= false then SetBacklightColor(255, 255, 255, "lhc") end

		OutputLCDMessage("", 0)
		ClearLCD()
	end,
	-- output a message to the gamepad
	out = function(message, timeout)
		OutputLCDMessage(message, timeout or 1000 * 60)
	end,
	-- colorize the output of a message to the gamepad
	text = function(self, color, ...)
		self.bg(color)
		self.out(...)
	end,
	-- coloured text
	black = function(self, ...)
		self:text({0, 0, 0}, ...)
	end,
	white = function(self, ...)
		self:text({255, 255, 255}, ...)
	end,
	grey = function(self, ...)
		self:text({127, 127, 127}, ...)
	end,
	red = function(self, ...)
		self:text({255, 0, 0}, ...)
	end,
	orange = function(self, ...)
		self:text({255, 127, 0}, ...)
	end,
	yellow = function(self, ...)
		self:text({255, 255, 0}, ...)
	end,
	green = function(self, ...)
		self:text({0, 255, 0}, ...)
	end,
	teal = function(self, ...)
		self:text({0, 255, 127}, ...)
	end,
	aqua = function(self, ...)
		self:text({0, 255, 255}, ...)
	end,
	blue = function(self, ...)
		self:text({0, 0, 255}, ...)
	end,
	purple = function(self, ...)
		self:text({127, 0, 255}, ...)
	end,
	fushia = function(self, ...)
		self:text({255, 0, 255}, ...)
	end
}

---------------------------
-- LOOP FUNCTIONS v3.2.0 --
---------------------------

local L = {
	-- internal loop counter
	i = -1,
	-- oscillate value flipped
	f = false,
	-- oscillate boolean value
	o = false,
	-- resets the loop counters
	new = function(self)
		self.i = -1
		self.f = false
		self.o = false
	end,
	-- increments a value while looping
	inc = function(self)
		self.i = self.i + 1

		return self.loop()
	end,
	-- increments and oscillates a value while looping
	osc = function(self, n)
		self.i = self.i + 1
		self.f = self.i % n == 0

		if self.f then self.o = not self.o end

		return self.loop()
	end,
	-- break out of loops using SHIFT
	loop = function()
		return not IsModifierPressed("lshift")
	end,
	-- break out of sleeps using SHIFT
	raw = function(ms)
		local max = time() + ms

		while time() < max do
			if IsModifierPressed("lshift") then break end

			Sleep(MS)
		end
	end,
	-- stop press events using SHIFT
	stop = function()
		return IsModifierPressed("lshift")
	end,
	-- PRETTY break out of sleeps using SHIFT
	wait = function(ms, reason, units)
		local display = (reason ~= false and reason ~= nil) and reason or "wait"
		local max = time() + ms
		local tail

		if reason ~= false and units ~= false then
			local number = ms < 1000 and ms
				or ms < 1000 * 60 and math.floor(ms / 10) / 100
				or math.floor(ms / (10 * 60)) / 100
			local unit = ms < 1000 and "ms"
				or ms < 1000 * 60 and "second"
				or "minute"

			if unit ~= "ms" and number ~= 1 then unit = unit .. "s" end

			tail = " " .. number .. " " .. unit
		end

		G:purple(G.center(display .. (tail or "")), ms)

		while time() < max do
			if IsModifierPressed("lshift") then break end

			Sleep(MS)
		end

		G.clear()
	end
}

----------------------------
-- MOUSE FUNCTIONS v3.5.0 --
----------------------------

local M = {
	-- move mouse cursor
	m = function(x, y)
		if type(x) == "table" then x, y = unpack(x) end

		MoveMouseRelative(x, y)
	end,
	-- press mouse button
	p = function(btn)
		if L.stop() then return end

		PressMouseButton(btn)
		Sleep(MS)
	end,
	-- get/set mouse position
	pos = function(x, y)
		if x == nil then return GetMousePosition() end
		if type(x) == "table" then x, y = unpack(x) end

		MoveMouseTo(x, y)
	end,
	-- press, hold, and release mouse button
	pr = function(btn, hold)
		if L.stop() then return end

		PressMouseButton(btn)

		if hold == nil then Sleep(MS) else L.wait(hold) end

		ReleaseMouseButton(btn)
		Sleep(MS)
	end,
	-- release mouse button
	r = function(btn)
		ReleaseMouseButton(btn)
		Sleep(MS)
	end
}

-------------------------------
-- KEYBOARD FUNCTIONS v3.3.0 --
-------------------------------

local K = {
	--  press or release the CTRL key
	ctrl = function(press)
		if press then
			PressKey("rctrl")
		else
			ReleaseKey("rctrl")
		end
	end,
	-- press and release the Enter key
	enter = function()
		PressKey("enter")
		Sleep(MS)
		ReleaseKey("enter")
		Sleep(MS)
	end,
	-- press key
	p = function(btn)
		if L.stop() then return end

		PressKey(btn)
		Sleep(MS)
	end,
	-- press, hold, and release key
	pr = function(btn, hold)
		if L.stop() then return end

		PressKey(btn)

		if hold == nil then Sleep(MS) else L.wait(hold) end

		ReleaseKey(btn)
		Sleep(MS)
	end,
	-- release key
	r = function(btn)
		ReleaseKey(btn)
		Sleep(MS)
	end,
	-- press or release the SHIFT key
	shift = function(press)
		if press then
			PressKey("rshift")
		else
			ReleaseKey("rshift")
		end
	end,
	-- type out a message
	type = function(message)
		local r = {
			["-"] = "minus",
			["/"] = "slash",
			[" "] = "spacebar"
		}

		local u = {
			["+"] = "equal"
		}

		for c in message:gmatch(".") do
			local code = u[c] or r[c] or c
			local shift = u[c] or c:match("%u")

			if L.stop() then return end
			if shift then PressKey("rshift") end

			PressKey(code)
			Sleep(MS)
			ReleaseKey(code)
			Sleep(MS)

			if shift then ReleaseKey("rshift") end
		end
	end
}

---------------------------
-- STEP FUNCTIONS v1.1.1 --
---------------------------

local S = {
	-- tracks what steps were chosen
	path = {},
	-- flag enabling the capturing of steps
	run = false,
	-- current step
	step = nil,
	-- steps to be configured
	steps = {},
	-- configures the step table
	configure = function(self, config)
		for key, value in pairs(config) do
			self[key] = value
		end
	end,
	-- function called when next reaches a value
	done = function(self, value) end,
	-- handles capturing events and stepping
	next = function(self, index)
		local next = self.run

		if next then
			table.insert(self.path, index)

			local step = (self.step or self.steps)[index]

				if type(step) == "table" then
				self.step = step
				self:options()
			elseif step ~= nil then
				self:done(step)
				self:reset()
			end
		end

		return next
	end,
	-- lists the available steps on the gamepad
	options = function(self)
		local display = ""
		local steps = self.step or self.steps

		for i = 1, 3 do
			local ii = i + 3
			local left = steps[i] or ""
			local right = ii == 6 and "cancel" or steps[ii] or ""

			if type(left) == "table" then
				left = left.label or #left .. " options"
			end

			if type(right) == "table" then
				right = right.label or #right .. " options"
			end

			if #left > 12 then left = left:sub(1, 10) .. "..." end
			if #right > 12 then right = right:sub(1, 10) .. "..." end

			display = display .. "G" .. i .. " " .. left .. "\t"
			display = display .. "G" .. ii .. " " .. right .. "\n"
		end

		G.clear(false)
		G:teal(display)
	end,
	-- reset the steps and disable capture
	reset = function(self, ms, reason)
		self.run = false
		self.step = nil

		if type(ms) == "number" then L.wait(ms, reason, false) end

		G.clear()
	end,
	-- enable/disable event capture for steps
	toggle = function(self)
		if self.run then
			self:reset(1000, "cancelled")
		else
			self.run = true
			self.path = {}
			self:options()
		end
	end
}

---------------------------
-- BIND FUNCTIONS v1.1.0 --
---------------------------

local B = {
	-- current select M-key
	M = GetMKeyState(),
	-- macro helper for pressing enter
	enter = {nil, "enter"},
	-- macro helper for pressing escape
	escape = {nil, "escape"},
	-- configures the bind table
	configure = function(self, config)
		for key, value in pairs(config) do
			self[key] = value
		end
	end,
	-- trigger one or more G-key events
	emit = function(keys)
		if keys.press or keys.release then
			keys = {keys}
		end

		for i, key in ipairs(keys) do
			if L.stop() then return end

			if key.press then
				key.press(key)
			end

			if key.release then
				key.release(key)
			end

			L.wait(50)
		end
	end,
	-- simplifies calling multiple macros
	--  - table {integer, integer} = M.pos()
	--  - table {nil, string} = K.pr()
	--  - integer < 10 = M.pr()
	--  - integer >= 10 = L.wait()
	--  - string = K.type()
	macro = function(inputs, delay)
		for key, value in pairs(inputs) do
			if type(value) == "table" then
				local x, y = unpack(value)

				if x == nil then K.pr(y)
				else M.pos(value) end
			elseif type(value) == "number" then
				if value < 10 then M.pr(value)
				else L.wait(value) end
			elseif type(value) == "string" then
				K.type(value)
			else
				local k, v = tostring(key), tostring(value)

				print("unknown key = " .. k .. ", value = " .. v .. "\n")
			end

			if type(delay) == "number" then L.raw(delay) end
		end
	end,
	-- called when profile is deactivated
	reset = function() end
}

---------------------
-- BINDINGS v1.0.0 --
---------------------

--
-- DATA
--

local data = {
	chat = {48505, 50142},
	confirm = {37825, 38711},
	far = {43844, 50597},
	front = {43690, 42081},
	host = {
		-- create lobby
		{25500, 48500},
		{18183, 43994},
		-- wait for server
		1500,
		-- select very hard
		{19975, 31151},
		{17876, 38483},
		-- select same random
		{19975, 33246},
		{17773, 38392},
		-- enable turbo
		{17235, 45997},
		-- make public
		{32550, 55060}
	},
	near = {43767, 47683},
	quit = {32883, 44586},
	ready = {50092, 55288},
	target = {43562, 59250},
	waits = {
		{3, 1000 * 30},
		{1, 1000 * 60},
		{0, 1000 * 30},
		{2, 1000 * 60 * 2},
		{2, 1000 * 60 * 3}
	}
}

--
-- STEPS
--

S:configure({
	steps = {
		{
			label = "1 min",
			"starting with +1 or in 1 min",
			"starting with +2 or in 1 min",
			"starting in 1 min"
		},
		{
			label = "2 min",
			"starting with +1 or in 2 min",
			"starting with +2 or in 2 min",
			"starting in 2 min"
		},
		{
			label = "30 sec",
			"starting with +1 or in 30 sec",
			"starting with +2 or in 30 sec",
			"starting in 30 sec"
		},
		{
			label = "4 min",
			"starting with +1 or in 4 min",
			"starting with +2 or in 4 min",
			"starting in 4 min",
		},
		{
			label = "5 min",
			"starting with +1 or in 5 min",
			"starting with +2 or in 5 min",
			"starting in 5 min",
			nil,
			""
		}
	},
	done = function(self, value)
		local restore = {M.pos()}

		B.macro({data.chat, 1, 50, value, B.enter, restore})
	end,
	-- add function to display lobby wait times
	type = function(self)
		if self.run then return end

		-- first = time to wait
		-- last = player count
		local first, last = unpack(self.path)
		-- next = next G-key integer to press
		-- wait = ms to wait before press
		local next, wait = unpack(data.waits[first])

		if next == nil or wait == nil then return end

		local debounce = wait + time()
		local skip = first == 5 and last == 5

		if not skip then L.wait(wait) end

		if skip or debounce - 100 < time() then
			if skip or next == 0 then
				B.macro({data.chat, 1, 50, "ready", B.enter, data.ready})

				G:green(G.center("ready"), 1000 * 30)

				L.raw(1000 * 30)

				B.macro({data.chat, 1, 50, "ready or kick 30 sec", B.enter, data.ready})

				L.wait(1000 * 30, "kick user in")

				G:red(G.center("kick user"), 1000 * 15)

				L.raw(1000 * 15)

				G.clear()
			else
				B.emit({B.M1.G6, B.M1["G" .. next], B.M1["G" .. last]})
			end
		end
	end
})

--
-- BINDS
--

B:configure({
	M1 = {
		-- host lobby
		G1 = {
			release = function()
				if S:next(1) then
					S:type()
				else
					local restore = {M.pos()}

					for i, value in ipairs(data.host) do
						if type(value) == "number" then
							L.wait(value)
						else
							B.macro({value, 1, 20})
						end
					end

					M.pos(restore)
				end
			end
		},
		-- remake game
		G2 = {
			release = function()
				if S:next(2) then
					S:type()
				else
					local restore = {M.pos()}

					B.macro({
						B.enter,
						"gg wp remake",
						B.enter,
						1250,
						B.escape,
						data.quit,
						1,
						data.confirm,
						1,
						restore
					})
				end
			end
		},
		-- front
		G3 = {
			release = function()
				if S:next(3) then
					S:type()
				else
					local restore = {M.pos()}

					B.macro({
						data.target,
						1,
						data.front,
						1,
						restore
					}, 20)
				end
			end
		},
		-- far
		G4 = {
			release = function()
				if S:next(4) then
					S:type()
				else
					local restore = {M.pos()}

					B.macro({
						data.target,
						1,
						data.far,
						1,
						restore
					}, 20)
				end
			end
		},
		-- near
		G5 = {
			release = function()
				if S:next(5) then
					S:type()
				else
					local restore = {M.pos()}

					B.macro({
						data.target,
						1,
						data.near,
						1,
						restore
					}, 20)
				end
			end
		},
		-- toggle step
		G6 = {
			release = function()
				S:toggle()
			end
		},
		-- spam ctrl+shift click
		G12 = {
			release = function()
				K.ctrl(true)
				K.shift(true)

				while L.loop() do
					M.pr(1)
				end

				K.shift(false)
				K.ctrl(false)
			end
		}
	},
	M2 = {
		-- gamepad testing
		G5 = {
			release = function()
				G.clear()
				G.out(string.rep("x", 30))
				G.out(string.rep("M", 16))
				G.out(string.rep(" ", 50) .. "i")
			end
		},
		-- log mouse position
		G6 = {
			press = function()
				OutputLogMessage("Mouse is at %d, %d\n", GetMousePosition())
			end
		}
	},
	reset = function()
		S:reset()
	end
})

-----------------------
-- DISPATCHER v3.5.0 --
-----------------------

function OnEvent(event, arg)
	local g = B["M" .. B.M] and B["M" .. B.M]["G" .. arg]
	local mouse = event == "MOUSE_BUTTON_PRESSED"
		or event == "MOUSE_BUTTON_RELEASED"
	local callback = event == "G_PRESSED" and "press"
		or event == "MOUSE_BUTTON_PRESSED" and "press"
		or event == "G_RELEASED" and "release"
		or event == "MOUSE_BUTTON_RELEASED" and "release"

	if event == "M_RELEASED" then B.M = arg end
	if event == "PROFILE_DEACTIVATED" then B:reset() end
	if g and g[callback] then g[callback](g, mouse) end
end
