function loop()
	return not IsModifierPressed("shift")
end

local key = PressAndReleaseKey
local time = GetRunningTime

-- BINDINGS --

local M = {
	key = 3,
	M3 = {
		-- press = B(change weapon firing modes)
		-- hold(600ms) = M + F1(instant resuply)
		G23 = {
			ms = 600,
			time = nil,
			press = function(g)
				g.time = time()
			end,
			release = function(g)
				if (time() - g.time) > g.ms then
					key("m")
					Sleep(20)
					key("f1")
				else
					key("b")
				end
			end
		}
	}
}

-- DISPATCHER --

function OnEvent(event, arg)
	local key = M["M"..M.key] and M["M"..M.key]["G"..arg]
	local callback = event == "G_PRESSED" and "press" or event == "G_RELEASED" and "release"
	if event == "M_RELEASED" then M.key = arg end
	if key and key[callback] then key[callback](key, M) end
end
