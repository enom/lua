
-----------------------------
-- GLOBAL VARIABLES v1.0.1 --
-----------------------------

-- milliseconds for loops and holds
local MS = 10

-- alias certain functions
local print = OutputLogMessage
local time = GetRunningTime

---------------------------
-- LOOP FUNCTIONS v3.2.0 --
---------------------------

local L = {
	-- internal loop counter
	i = -1,
	-- oscillate value flipped
	f = false,
	-- oscillate boolean value
	o = false,
	-- resets the loop counters
	new = function(self)
		self.i = -1
		self.f = false
		self.o = false
	end,
	-- increments a value while looping
	inc = function(self)
		self.i = self.i + 1

		return self.loop()
	end,
	-- increments and oscillates a value while looping
	osc = function(self, n)
		self.i = self.i + 1
		self.f = self.i % n == 0

		if self.f then self.o = not self.o end

		return self.loop()
	end,
	-- break out of loops using SHIFT
	loop = function()
		return not IsModifierPressed("lshift")
	end,
	-- break out of sleeps using SHIFT
	raw = function(ms)
		local max = time() + ms

		while time() < max do
			if IsModifierPressed("lshift") then break end

			Sleep(MS)
		end
	end,
	-- stop press events using SHIFT
	stop = function()
		return IsModifierPressed("lshift")
	end,
	-- PRETTY break out of sleeps using SHIFT
	wait = function(ms, reason, units)
		local display = (reason ~= false and reason ~= nil) and reason or "wait"
		local max = time() + ms
		local tail

		if reason ~= false and units ~= false then
			local number = ms < 1000 and ms
				or ms < 1000 * 60 and math.floor(ms / 10) / 100
				or math.floor(ms / (10 * 60)) / 100
			local unit = ms < 1000 and "ms"
				or ms < 1000 * 60 and "second"
				or "minute"

			if unit ~= "ms" and number ~= 1 then unit = unit .. "s" end

			tail = " " .. number .. " " .. unit
		end

		G:purple(G.center(display .. (tail or "")), ms)

		while time() < max do
			if IsModifierPressed("lshift") then break end

			Sleep(MS)
		end

		G.clear()
	end
}

------------------------------
-- GAMEPAD FUNCTIONS v1.1.0 --
------------------------------

local G = {
	-- changes gamepad background color
	bg = function(r, g, b)
		if type(r) == "table" then r, g, b = unpack(r) end

		SetBacklightColor(r, g, b, "lhc")
	end,
	-- properly spaces a message to center it on the gamepad
	center = function(message)
		return "\n" .. string.rep(" ", (50 - (#message * 1.618)) / 2) .. message .. "\n"
	end,
	-- clear gamepad color and text
	clear = function(color)
		if color ~= false then SetBacklightColor(255, 255, 255, "lhc") end

		OutputLCDMessage("", 0)
		ClearLCD()
	end,
	-- output a message to the gamepad
	out = function(message, timeout)
		OutputLCDMessage(message, timeout or 1000 * 60)
	end,
	-- colorize the output of a message to the gamepad
	text = function(self, color, ...)
		self.bg(color)
		self.out(...)
	end,
	-- coloured text
	black = function(self, ...)
		self:text({0, 0, 0}, ...)
	end,
	white = function(self, ...)
		self:text({255, 255, 255}, ...)
	end,
	grey = function(self, ...)
		self:text({127, 127, 127}, ...)
	end,
	red = function(self, ...)
		self:text({255, 0, 0}, ...)
	end,
	orange = function(self, ...)
		self:text({255, 127, 0}, ...)
	end,
	yellow = function(self, ...)
		self:text({255, 255, 0}, ...)
	end,
	green = function(self, ...)
		self:text({0, 255, 0}, ...)
	end,
	teal = function(self, ...)
		self:text({0, 255, 127}, ...)
	end,
	aqua = function(self, ...)
		self:text({0, 255, 255}, ...)
	end,
	blue = function(self, ...)
		self:text({0, 0, 255}, ...)
	end,
	purple = function(self, ...)
		self:text({127, 0, 255}, ...)
	end,
	fushia = function(self, ...)
		self:text({255, 0, 255}, ...)
	end
}

-------------------------------
-- KEYBOARD FUNCTIONS v3.3.1 --
-------------------------------

local K = {
	--  press or release the CTRL key
	ctrl = function(press)
		if press then
			PressKey("rctrl")
		else
			ReleaseKey("rctrl")
		end
	end,
	-- press and release the Enter key
	enter = function()
		PressKey("enter")
		Sleep(MS)
		ReleaseKey("enter")
		Sleep(MS)
	end,
	-- press key
	p = function(btn)
		if L.stop() then return end

		PressKey(btn)
		Sleep(MS)
	end,
	-- press, hold, and release key
	pr = function(btn, hold)
		if L.stop() then return end

		PressKey(btn)

		if hold == nil then Sleep(MS) else L.wait(hold) end

		ReleaseKey(btn)
		Sleep(MS)
	end,
	-- release key
	r = function(btn)
		ReleaseKey(btn)
		Sleep(MS)
	end,
	-- press or release the SHIFT key
	shift = function(press)
		if press then
			PressKey("rshift")
		else
			ReleaseKey("rshift")
		end
	end,
	-- type out a message
	type = function(message)
		local r = {
			["-"] = "minus",
			[";"] = "semicolon",
			["/"] = "slash",
			[" "] = "spacebar",
			["="] = "equal"
		}

		local u = {
			["~"] = "tilde",
			["!"] = "1",
			["@"] = "2",
			["#"] = "3",
			["$"] = "4",
			["%"] = "5",
			["^"] = "6",
			["&"] = "7",
			["*"] = "8",
			["("] = "9",
			[")"] = "0",
			["_"] = "minus",
			["+"] = "equal"
		}

		for c in message:gmatch(".") do
			local code = u[c] or r[c] or c
			local shift = u[c] or c:match("%u")

			if L.stop() then return end
			if shift then PressKey("rshift") end

			PressKey(code)
			Sleep(MS)
			ReleaseKey(code)
			Sleep(MS)

			if shift then ReleaseKey("rshift") end
		end
	end
}

----------------------------
-- MOUSE FUNCTIONS v3.5.0 --
----------------------------

local M = {
	-- move mouse cursor
	m = function(x, y)
		if type(x) == "table" then x, y = unpack(x) end

		MoveMouseRelative(x, y)
	end,
	-- press mouse button
	p = function(btn)
		if L.stop() then return end

		PressMouseButton(btn)
		Sleep(MS)
	end,
	-- get/set mouse position
	pos = function(x, y)
		if x == nil then return GetMousePosition() end
		if type(x) == "table" then x, y = unpack(x) end

		MoveMouseTo(x, y)
	end,
	-- press, hold, and release mouse button
	pr = function(btn, hold)
		if L.stop() then return end

		PressMouseButton(btn)

		if hold == nil then Sleep(MS) else L.wait(hold) end

		ReleaseMouseButton(btn)
		Sleep(MS)
	end,
	-- release mouse button
	r = function(btn)
		ReleaseMouseButton(btn)
		Sleep(MS)
	end
}

---------------------------
-- BIND FUNCTIONS v1.2.0 --
---------------------------

local B = {
	-- current select M-key
	M = GetMKeyState(),
	-- configures the bind table
	configure = function(self, config)
		for key, value in pairs(config) do
			self[key] = value
		end
	end,
	-- trigger one or more G-key events
	emit = function(keys)
		if keys.press or keys.release then
			keys = {keys}
		end

		for i, key in ipairs(keys) do
			if L.stop() then return end

			if key.press then
				key.press(key)
			end

			if key.release then
				key.release(key)
			end

			L.wait(50)
		end
	end,
	-- simplifies calling multiple macros
	--  - table {integer, integer} = M.pos(x, y)
	--  - table {string, integer?} = K.pr(key, hold?)
	--  - table {string, boolean} = K.p(key) or K.r(key)
	--  - integer < 10 = M.pr()
	--  - integer >= 10 = L.wait()
	--  - string = K.type()
	macro = function(inputs, delay)
		for key, value in pairs(inputs) do
			if type(value) == "table" then
				local k, h = unpack(value)

				if type(k) == "string" then 
					if h == nil or type(h) == "number" then
						K.pr(k, h)
					else
						if h then K.p(k) else K.r(k) end
					end
				else M.pos(value) end
			elseif type(value) == "number" then
				if value < 10 then M.pr(value)
				else L.wait(value) end
			elseif type(value) == "string" then
				K.type(value)
			else
				local k, v = tostring(key), tostring(value)

				print("unknown key = " .. k .. ", value = " .. v .. "\n")
			end

			if type(delay) == "number" then L.raw(delay) end
		end
	end,
	-- called when profile is deactivated
	reset = function() end
}

---------------------
-- BINDINGS v1.0.0 --
---------------------

B:configure({
	M1 = {
		G9 = {},
		G10 = {},
		G11 = {},
		G12 = {},
		G13 = {},
		G14 = {},
		G15 = {},
		G16 = {},
		G17 = {},
		G18 = {
			release = function()
				local remove = {39311, 52692}
				B.macro({
					-- muzzle
					{10756, 31105}, 1, 1,
					remove, 1, 1,
					-- attachment 1
					{11038, 39075}, 1, 1,
					remove, 1, 1,
					-- attachment 2
					{10961, 42718}, 1, 1,
					remove, 1, 1,
					-- attachment 3
					{10705, 46817}, 1, 1,
					remove, 1, 1,
					-- attachment 4
					{10423, 50734}, 1, 1,
					remove, 1, 1,
					-- stock
					{11012, 11659}, 1, 1,
					remove, 1, 1,
					-- grip
					{10910, 15530}, 1, 1,
					remove, 1, 1,
					-- handguard
					{10910, 19674}, 1, 1,
					remove, 1, 1,
					-- barrel
					{10858, 23409}, 1, 1,
					remove, 1, 1,
					-- magazine
					{10705, 27143}, 1, 1,
					remove, 1, 1,
					-- scope
					{10884, 34703}, 1, 1,
					remove, 1, 1
				}, 15)
			end
		},
		G19 = {},
		G20 = {
			release = function()
				local x, y = M.pos()
				print(x .. ", " .. y .. "\n")
			end
		}
	}
})

-----------------------
-- DISPATCHER v3.5.0 --
-----------------------

function OnEvent(event, arg)
	local g = B["M" .. B.M] and B["M" .. B.M]["G" .. arg]
	local mouse = event == "MOUSE_BUTTON_PRESSED"
		or event == "MOUSE_BUTTON_RELEASED"
	local callback = event == "G_PRESSED" and "press"
		or event == "MOUSE_BUTTON_PRESSED" and "press"
		or event == "G_RELEASED" and "release"
		or event == "MOUSE_BUTTON_RELEASED" and "release"

	if event == "M_RELEASED" then B.M = arg end
	if event == "PROFILE_DEACTIVATED" then B:reset() end
	if g and g[callback] then g[callback](g, mouse) end
end
