-- WHILE --

function loop()
	return not IsModifierPressed("shift")
end

-- BINDINGS --

local M = {
	key = GetMKeyState(),
	M1 = {
		G4 = {
			press = function()
				while loop() do
					PressKey("f")
					Sleep(5)
					ReleaseKey("f")
				end
			end
		},
		G5 = {
			press = function()
				while loop() do
					PressMouseButton(1)
					Sleep(5)
					ReleaseMouseButton(1)
				end
			end
		},
		G6 = {
			press = function()
				while loop() do
					PressMouseButton(1)
					Sleep(1000)
					ReleaseMouseButton(1)
				end
			end
		}
	}
}

-- DISPATCHER --

function OnEvent(event, arg)
	local key = M["M"..M.key] and M["M"..M.key]["G"..arg]
	local callback = event == "G_PRESSED" and "press" or event == "G_RELEASED" and "release"
	if event == "M_RELEASED" then M.key = arg end
	if key and key[callback] then key[callback](key, M) end
end
