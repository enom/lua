-- GLOBAL VARIABLES v1.1.0 --

--[[
 - Milliseconds for loops and holds.
 -
 - @type int
 -]]
local MS = 10

-- global shortcuts
local print = OutputLogMessage
local time = GetRunningTime

-- LOOP FUNCTIONS v3.2.0 --

local l = {
	-- internal loop counter
	i = -1,
	-- oscillate value flipped
	f = false,
	-- oscillate boolean value
	o = false,
	-- resets the loop counters
	new = function(self)
		self.i = -1
		self.f = false
		self.o = false
	end,
	-- increments a value while looping
	inc = function(self)
		self.i = self.i + 1
		return self.loop()
	end,
	-- increments and oscillates a value while looping
	osc = function(self, n)
		self.i = self.i + 1
		self.f = self.i % n == 0
		if self.f then self.o = not self.o end
		return self.loop()
	end,
	-- break out of loops using SHIFT
	loop = function()
		return not IsModifierPressed("lshift")
	end,
	-- stop press events using SHIFT
	stop = function()
		return IsModifierPressed("lshift")
	end,
	-- break out of sleeps using SHIFT
	wait = function(ms)
		local now = time()
		local max = now + ms
		for i = now, max, MS do
			if IsModifierPressed("lshift") then break end
			if time() > max then break end
			Sleep(MS)
		end
	end
}

-- MOUSE FUNCTIONS v3.3.2 --

local m = {
	-- move mouse cursor
	m = function(x, y)
		MoveMouseRelative(x, y)
	end,
	-- press mouse button
	p = function(btn)
		if l.stop() then return end
		PressMouseButton(btn)
		Sleep(MS)
	end,
	-- get/set mouse position
	pos = function(x, y)
		if x == nil and y == nil then return GetMousePosition() end
		MoveMouseTo(x, y)
	end,
	-- press, hold, and release mouse button
	pr = function(btn, hold)
		if l.stop() then return end
		PressMouseButton(btn)
		if hold == nil then Sleep(MS) else l.wait(hold) end
		ReleaseMouseButton(btn)
		Sleep(MS)
	end,
	-- release mouse button
	r = function(btn)
		ReleaseMouseButton(btn)
		Sleep(MS)
	end
}

-- KEYBOARD FUNCTIONS v3.2.0 --

local k = {
	-- press key
	p = function(btn)
		if l.stop() then return end
		PressKey(btn)
		Sleep(MS)
	end,
	-- press, hold, and release key
	pr = function(btn, hold)
		if l.stop() then return end
		PressKey(btn)
		if hold == nil then Sleep(MS) else l.wait(hold) end
		ReleaseKey(btn)
		Sleep(MS)
	end,
	-- release key
	r = function(btn)
		ReleaseKey(btn)
		Sleep(MS)
	end,
	-- type out a message
	type = function(message)
		local r = {
			["-"] = "minus",
			["/"] = "slash",
			[" "] = "spacebar"
		}

		local u = {
			["+"] = "equal"
		}

		for c in message:gmatch(".") do
			local code = u[c] or r[c] or c
			local shift = u[c] or c:match("%u")

			if l.stop() then return end
			if shift then PressKey("rshift") end

			PressKey(code)
			Sleep(MS)
			ReleaseKey(code)
			Sleep(MS)

			if shift then ReleaseKey("rshift") end
		end
	end
}

-- BINDINGS v1.1.0 --

local V = {
	set = false,
	step = nil,
	steps = {
		"foods",
		"minerals",
		"enemies",
		"shelters"
		-- max 5
	},
	labels = {
		foods = {
			"Raspberries",
			"Mushrooms"
		},
		minerals = {
			"Copper",
			"Tin"
		},
		enemies = {
			"Burial Chambers"
		},
		shelters = {
		}
	},
	offsets = {
		foods = {58877, 52464},
		minerals = {58877, 52464},
		enemies = {58851, 48548},
		shelters = {58825, 40760}
	},
	reset = function(self)
		self.set = false
		self.step = nil
	end,
	marker = function(self, index)
		local marked = self.set

		if self.set then
			if self.step == nil then
				self.step = self.steps[index]
			else
				local label = self.labels[self.step][index]
				local x, y = m.pos()
				local offset = self.offsets[self.step]

				m.pos(unpack(offset))
				m.pr(1)

				m.pos(x, y)
				m.pr(1)
				m.pr(1)

				k.type(label)
				k.pr("enter")

				self:reset()
			end
		end

		return marked
	end,
	toggle = function(self)
		self.set = not self.set
		if not self.set then self:reset() end
	end
}

local M = {
	key = GetMKeyState(),
	M1 = {
		-- item crafting loop
		G1 = {
			crafting = 2150,
			release = function(g)
				if not V:marker(1) then
					while l.loop() do
						m.pr(1)
						l.wait(g.crafting)
					end
				end
			end
		},
		-- cook all
		G2 = {
			e = 60,
			px = 25,
			press = function()
				l:new()
			end,
			release = function(g)
				if not V:marker(2) then
					while l:osc(g.e) do
						k.pr("e")
						m.m(l.o and g.px or 0 - g.px, 0)
					end
				end
			end
		},
		-- plant farm
		G3 = {
			rows = 14,
			spacing = 400,
			stamina = 1200,
			press = function()
				l:new()
			end,
			release = function(g)
				if not V:marker(3) then
					while l:osc(g.rows) do
						if l.f and l.i > 0 then
							m.pr(1)
							k.pr("s", g.spacing)
							l.wait(g.stamina)
						end
						m.pr(1)
						k.pr(l.o and "d" or "a", g.spacing)
						l.wait(g.stamina)
					end
				end
			end
		},
		-- collect farm
		G4 = {
			rows = 7,
			spacing = 400,
			press = function()
				l:new()
			end,
			release = function(g)
				if not V:marker(4) then
					while l:osc(g.rows * 4) do
						if l.f and l.i > 0 then
							k.pr("e")
							k.pr("w", g.spacing)
						end
						k.pr("e")
						k.pr(l.o and "d" or "a", g.spacing / 4)
					end
				end
			end
		},
		-- mine all
		G5 = {
			swing = 200,
			release = function(g)
				if not V:marker(5) then
					while l.loop() do
						m.pr(1)
						l.wait(g.swing)
					end
				end
			end
		},
		-- map markers
		G6 = {
			release = function()
				V:toggle()
			end
		},
		-- repair all
		G12 = {
			release = function()
				while l.loop() do
					m.pr(1)
				end
			end
		},
		-- fill stations
		G14 = {
			release = function()
				while l.loop() do
					k.pr("e")
				end
			end
		}
	},
	M2 = {
		G6 = {
			release = function()
				print("%d, %d\n", m.pos())
			end
		}
	}
}

-- DISPATCHER v3.3.0 --

function OnEvent(event, arg)
	local key = M["M"..M.key] and M["M"..M.key]["G"..arg]
	local mouse = event == "MOUSE_BUTTON_PRESSED"
		or event == "MOUSE_BUTTON_RELEASED"
	local callback = event == "G_PRESSED" and "press"
		or event == "MOUSE_BUTTON_PRESSED" and "press"
		or event == "G_RELEASED" and "release"
		or event == "MOUSE_BUTTON_RELEASED" and "release"
	if event == "M_RELEASED" then M.key = arg end
	if key and key[callback] then key[callback](key, mouse) end
end
