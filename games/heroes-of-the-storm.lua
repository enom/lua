-- Clean up existing log
ClearLog()

-- Shortcuts --
local p = OutputLogMessage
local o = OutputLCDMessage

-- Default colors
local _cDefault = {72, 208, 154, "lhc"}
local _cDangerPing = {255, 0, 0, "lhc"}
local _cRewind = {0, 255, 0, "lhc"}

-- Dynamic binding object --
-- Lookup table holding all of our bindings
on = {}

local _m = {
	__index = function(self, key)
		-- p("\n\n_meta.__index("..key..")")

		if rawget(self, key) ~= nil then
			return self[key]
		end

		self[key] = {
			key = key,
			state = "RELEASED",
			-- Dynamically bind a press or release function
			bind = function(case, callback)
				-- p("\n_meta.bind("..case..", "..key..")")

				-- Reference to our own object
				local this = self[key]

				-- Release allows us to trigger release after the press function
				-- executes
				this[case] = function(release)
					-- p("\n_meta.bind() internal rigging")

					if case == "press" then
						this.state = "PRESSED"
					end

					if case == "release" then
						this.state = "RELEASED"
					end

					callback(this)

					if release == true then
						this.release()
					end
				end
			end,
			-- Default press function which also allows automatic release
			-- Updated the current key state
			press = function(release)
				-- p("\n"..key.." default press()")

				self[key].state = "PRESSED"

				if release == true then
					self[key].release()
				end
			end,
			-- Default release function just updates the key state
			release = function()
				-- p("\n"..key.." default release()")

				self[key].state = "RELEASED"
			end
		}

		return self[key]
	end,
}

setmetatable(on, _m)

-- Mouse and keyboard handler --
Q = function(ms)
	local _ms = ms or 10

	local _mouse = {}
	local _mouseHeld = {}
	local _mouseCapture = {}

	local _key = {}
	local _keyHeld = {}

	-- Key functions
	_key.press = function(...)
		p("\nQ().k.press()")

		PressAndReleaseKey(...)
		Sleep(_ms)

		return _key
	end

	_key.hold = function(...)
		p("\nQ().k.hold()")

		for k, v in ipairs({...}) do
			table.insert(_keyHeld, v)
		end

		PressKey(...)
		Sleep(_ms)

		return _key
	end

	_key.release = function(...)
		-- p("\nQ().k.release()")

		if #_keyHeld == 0 then
			return _key
		end

		local args = {...}

		if #args == 0 then
			p("\nQ().k.release() previous")

			-- Release the last key being held
			ReleaseKey(table.remove(_keyHeld))
		elseif args[1] == true then
			p("\nQ().k.release() all")

			-- Release all keys being held
			ReleaseKey(unpack(_keyHeld))

			_keyHeld = {}
		else
			p("\nQ().k.release() specific")
			local keys = {}

			for i = 1, #args do
				-- Look through arguments provided
				for k, v in ipairs(_keyHeld) do
					-- Loop through all keys held to remove the ones specified
					if v == args[i] then
						table.insert(keys, table.remove(_keyHeld, k))
					end
				end
			end

			-- Do mass unsetting via Logitech's API
			ReleaseKey(unpack(keys))
		end

		Sleep(_ms)

		return _key
	end

	_key.run = function(...)
		p("\nQ().k.run()")

		local args = {...}

		for i = 1, #args do
			args[i](_key)
		end

		return _key
	end

	_key.sleep = function(ms)
		p("\nQ().k.sleep(%d)", ms)

		Sleep(ms)

		return _key
	end

	-- Mouse functions
	_mouse.press = function(btn)
		p("\nQ().m.press(%i)", btn)

		PressAndReleaseMouseButton(btn)
		Sleep(_ms)

		return _mouse
	end

	_mouse.hold = function(btn)
		p("\nQ().m.hold(%i)", btn)

		table.insert(_mouseHeld, btn)

		PressMouseButton(btn)
		Sleep(_ms)

		return _mouse
	end

	_mouse.release = function(btn)
		-- p("\nQ().m.release()")
		-- No keys held to release
		if #_mouseHeld == 0 then
			return _mouse
		end

		if btn == nil then
			-- Release previous key pressed
			p("\nQ().m.release() previous ".._mouseHeld[#_mouseHeld])

			-- Does an array.pop and returns the keycode to remove
			ReleaseMouseButton(table.remove(_mouseHeld))
		elseif btn == true then
			-- Release all keys pressed
			p("\nQ().m.release() all")

			for i = #_mouseHeld, 1, - 1 do
				-- Traverse held keys backwards and release them
				ReleaseMouseButton(_mouseHeld[i])
			end

			-- Reset our array
			_mouseHeld = {}
		elseif _mouseHeld[btn] ~= nil then
			-- Release one key pressed
			p("\nQ().m.release() key %s", btn)

			for i, v in ipairs(_mouseHeld) do
				-- Find the right key index to remove from our array and release that key
				if v == btn then
					ReleaseMouseButton(table.remove(_mouseHeld, i))
				end
			end
		end

		Sleep(_ms)

		return _mouse
	end

	_mouse.capture = function(id)
		p("\nQ().m.capture(%s) x = %d, y = %d", id, GetMousePosition())

		local x, y = 100, 200

		_mouseCapture[id] = {GetMousePosition()}

		return _mouse
	end

	_mouse.move = function(x, y, relative)
		-- p("\nQ().m.move()")

		local c = _mouseCapture

		if type(x) == "string" and c[x] ~= nil then
			-- Move to a saved capture
			p("\nQ().m.move() to capture %s", x)

			MoveMouseTo(c[x][1], c[x][2])
		elseif relative == true then
			-- Move relative to current cursor position
			p("\nQ().m.move() relative %d, %d", x, y)

			local _x, _y = GetMousePosition()
			MoveMouseTo(_x + x, _y + y)
		else
			-- Move to absolute position
			p("\nQ().m.move() absolute")

			MoveMouseTo(x, y)
		end

		Sleep(_ms)

		return _mouse
	end

	_mouse.run = function(...)
		p("\nQ().m.run()")

		local args = {...}

		for i = 1, #args do
			args[i](_mouse)
		end

		return _mouse
	end

	_mouse.sleep = function(ms)
		p("\nQ().m.sleep() %d", ms)

		Sleep(ms)

		return _mouse
	end

	-- Add ability to jump between the two
	_key.m = _mouse
	_mouse.k = _key

	return {
		k = _key,
		m = _mouse
	}
end


-- Do actual binding --

--[[
-- Fast danger ping
--
-- Press alt, move mouse up to danger ping, release alt, and return
-- the cursor back to its initial position
--]]
on.G15.bind("press", function(key, on)
	p("\nG15 Danger Ping")

	ClearLCD()
	SetBacklightColor(unpack(_cDangerPing))
	o("Danger Ping")


	Q()
	.k.hold("lalt")
	.m .capture("origin")
	.hold(1)
	.move(0, - 5000, true)
	.sleep(50)
	.release()
	.sleep(50)
	.move("origin")
	.k.release()

	-- Sleep(150)
	SetBacklightColor(unpack(_cDefault))
end)

--[[
-- Rewind Decoy Snipe
--
-- Snipe, decoy, cripple, rewind, snipe, decoy, cripple.
--
-- Can be canceled at any time by hitting the same GKey again.
--]]
on.G13.bind("press", function(key, on)
	p("\nG13 Rewind Decoy Snip")

	ClearLCD()
	o("Rewind Decoy Snipe")

	SetBacklightColor(unpack(_cDangerPing))

	--

	SetBacklightColor(unpack(_cDefault))
end)

on.done = true

-- Initialize listener --
function OnEvent(event, arg)
	p("\nevent = %s, arg = %s\n", event, arg)

	local key, type = string.match(event, "^(%a+)[_A-Z]*_(%a+)$")

	if key == nil or type == nil or key == "PROFILE" then
		return
	end

	local ref = key .. arg

	-- p(rawget(on, "G22"))

	if rawget(on, ref) ~= nil then
		if type == "PRESSED" then
			on[ref].press()
		elseif type == "RELEASED" then
			on[ref].release()
		end
	end
end
