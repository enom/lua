-- t = turtle;

--[[
function print(message)
	OutputLogMessage(message .. "\n");
	OutputLCDMessage(message .. "\n");
end
--]]

--Explode function, works like PHP
function explode ( seperator, str )
	local pos, arr = 0, {}
	for st, sp in function() return string.find( str, seperator, pos, true ) end do -- for each divider found
		table.insert( arr, string.sub( str, pos, st - 1 ) ) -- Attach chars left of current divider
		pos = sp + 1 -- Jump past current divider
	end
	table.insert( arr, string.sub( str, pos ) ) -- Attach chars right of last divider
	return arr
end

function typeof(var)
	local _type = type(var);
	if(_type ~= "table" and _type ~= "userdata") then
		return _type;
	end
	local _meta = getmetatable(var);
	if(_meta ~= nil and _meta._NAME ~= nil) then
		return _meta._NAME;
	else
		return _type;
	end
end

function Miner(arguments)
	local program = {
		-- Slot counters
		_s1, _s2, _s3 = 1, 1, 1,
		_s4, _s5, _s6 = 1, 1, 1,
		_s7, _s8, _s9 = 1, 1, 1,
		-- Param counters
		_d, _t = 0, 0,

		_params = {
			-- Max depth
			depth = 64,
			-- Sides to mine
			sides = nil,
			-- When to place a torch
			torch = nil,
		},

		--[[
		 - Prints a unix like help message to assist
		 - in configuring our Miner.
		 -
		 - @return void
		 -]]
		help = function(self)
			local s = "Miner v1\n" ..
			"Usage: Miner [OPTION]...\n" ..
			"\n" ..
			"Description:\n" ..
			"  An automated mining turtle that will mine\n" ..
			"  specific block types for a certain distance.\n" ..
			"  The turtle will automatically return home\n" ..
			"  when one its slots has a full stack of 64 ore." ..
			"\n" ..
			"  Each time the turtle goes to mine forward or\n" ..
			"  one of the sides, it will compare every slot\n" ..
			"  it holds, except for slot 1 and 2(top left, top\n" ..
			"  mid), against the block to mine. Place the ore\n" ..
			"  blocks -- not the actual drops(Ex: redstone) --\n" ..
			"  you want the turtle to mine in every other slot.\n" ..
			"\n" ..
			"  The first slot is the junk slot. This one is\n" ..
			"  emptied each time it digs a block not specified\n" ..
			"  within the inventory. The second slot is for\n" ..
			"  torches\n" ..
			"\n" ..
			"Options:\n" ..
			"  depth=MAX\n" ..
			"    The number of blocks you want the turtle\n" ..
			"    to move forward. Be mindeful of the despawn\n" ..
			"    distance, ~250 blocks from player. Defaults to 64.\n" ..
			"\n" ..
			"  sides=SIDES\n" ..
			"    SIDES is an array of strings for the turtle\n" ..
			"    to mine each time it moves forward. Sides\n" ..
			"    are 'up', 'left', 'right', and 'down'.\n" ..
			"    Ex: sides={'up', 'right'}\n" ..
			"\n" ..
			"  torch=PLACE\n" ..
			"    PLACE is an array taking two argumrnts.\n" ..
			"    The first is the direction the torch will\n" ..
			"    be placed and the second is interval at\n" ..
			"    which the torch should be placed.\n" ..
			"    Directions are 'up', 'left', 'right', \n" ..
			"    and'down'.\n" ..
			"    Ex: torch={'up', 7}\n" ..
			"\n";

			print(s);
		end,

		--[[
		 - Does a verification at each step forward
		 - to make sure we don't over mine an ore. In
		 - addition we check to see if the turtle has
		 - run out of torches to place, given the user
		 - set the Miner to place them.
		 -
		 - @return boolean Any one of our slots is full, or torches are empty
		 -]]
		continue = function(self)
			torch = self._params.torch;

			-- Slot 1 is junk, slot 2 is torches, others are ore to mine
			for i = 3, 9 do
				if t.getItemCount(i) == 64 then
					return false; end
				end

				-- Check if we're out of torches
				if torch ~= nil and t.getItemCount(2) == 0 then
					return false; end

					if self._d == self._params.depth then
						return false; end

						return true;
					end,

					--[[
		 - Validates and saves how deep the turtle should
		 - mine before coming back home.
		 -
		 - @param integer depth Number of blocks from start
		 - @return void
		 -]]
					setDepth = function(self, depth)
						if type(depth) ~= "number" then
							print("Parameter depth requires a number.\nTry 'Miner help' for more information\n");
						else
							print("Setting depth to " .. depth);

							self._params.depth = depth;
						end
					end,

					--[[
		 - Validates and saves which sides will be mined when
		 - digging forward.
		 -
		 - @param table sides Table of strings with each direction
		 - @return void
		 -]]
					setSides = function(self, sides)
						if type(sides) ~= "table" then
							print("Parameter sides requires a table.\nTry 'Miner help' for more information\n");
						else
							local s = "";

							for k, v in pairs(sides) do
								if type(v) ~= "string" then
									print("Invalid argument for sides at index " .. k .. "\n");
									return;
								end

								if s ~= "" then
									s = s .. ", " end
									s = s .. v
								end

								print("Setting turtle to mine " .. s);

								self._params.sides = sides;
							end
						end,

						--[[
		 - Validates and saves the torch setting. The torch
		 - table should contain the direction as a string for
		 - the first value and the number of blocks seperating
		 - each torch emplacement.
		 -
		 - @param table each Table containing the direction and interval
		 - @return void
		 -]]
						setTorches = function(self, each)
							if type(each) ~= "table" then
								print("Parameter torches requires a table.\nTry 'Miner help' for more information\n");
							else
								if type(each[1]) ~= "string" then
									print("Invalid first argument for torch.\nTry 'Miner help' for more information\n");
								elseif type(each[2]) ~= "number" then
									print("Invalid second argument for torch.\nTry 'Miner help' for more information\n");
								else
									print("Setting torch " .. each[1] .. " at every " .. each[2] .. " blocks");

									self._params.torch = each;
								end
							end
						end,

						--[[
		 - Converts the parameters passed to our Miner
		 - into useable values and implements them. If there
		 - sides isn't specified, the turtle will simply
		 - mine forward in a 1x1 block shaft.
		 -
		 - @param table args Arguments passed from command line
		 - @return void
		 -]]
						setParams = function(self, args)
							for key, val in ipairs(args) do
								if val == "help" then
									self:help();
									return;
								else
									assert(loadstring(val))();
								end
							end

							if type(depth) ~= nil then
								self:setDepth(depth); end

								if type(sides) ~= nil then
									self:setSides(sides); end

									if type(torch) ~= nil then
										self:setTorches(torch); end
									end,

									loop = function(self)
										local sides = self._params.sides;

										if self:continue() then
											if sides ~= nil then
												for key, val in sides do
													if val == "up" then
														self:up();
													end

													if val == "down" then
														self:down();
													end

													if val == "left" then
														self:left();
													end

													if val == "right" then
														self:right();
													end
												end
											end

											self:forward();
										else
											self:home()
										end
									end,

									--[[
		 - Places a torch above the turtle to light
		 - up the tunnel. The turtle will automatically
		 - dig upwards and place the torch there.
		 -
		 - @return void
		 -]]
									torch = function(self)
										local each = self._params.torch[1];
										local place = self._params.torch[2];

										if each ~= 0 then
											-- If we reach the block count for each torch placement
											if self._t == each then
												-- Slot 1 is junk
												-- Slot 2 is torches
												t.select(1);

												if place == "up" then
													t.digUp();

													t.select(2);
													t.placeUp()
												end

												if place == "down" then
													t.digDown();

													t.select(2);
													t.placeDown();
												end

												if place == "right" then
													t.turnRight();
													t.dig()

													t.select(2);
													t.place();

													t.turnLeft();
												end

												if place == "left" then
													t.turnLeft();
													t.dig()

													t.select(2);
													t.place();

													t.turnRight();
												end

												t.select(1);
												t.drop();
											else
												each = each + 1;
											end
										end
									end,

									--[[
		 - Moves the turtle forward by one block. If
		 - the block in front is an ore then the
		 - turtle won't drop it.
		 -
		 - @return void
		 -]]
									forward = function(self)
										if self:check() then
											t.dig();
										else
											-- Slot 1 is junk
											t.select(1);

											t.dig();
											t.drop();
										end

										self._d = self._d + 1

										print("Moving forward; current depth at " .. self._d);
									end,

									--[[
		 - Turns the turtle to the right, digs the
		 - block if it's an ore we want, then resets
		 - the turtle's direction.
		 -
		 - @return void
		 -]]
									right = function(self)
										t.turnRight();

										if self:check() then
											t.dig();
										end

										t.turnLeft();
									end,

									--[[
		 - Turns the turtle to the left, digs the
		 - block if it's an ore we want, then resets
		 - the turtle's direction.
		 -
		 - @return void
		 -]]
									left = function(self)
										t.turnLeft();

										if self:check() then
											t.dig();
										end

										t.turnRight();
									end,

									--[[
		 - Check's the block above the turtle and digs
		 - it if it's an ore we want.
		 -
		 - @return void
		 -]]
									up = function(self)
										if self:check("up") then
											t.digUp();
										end
									end,

									--[[
		 - Check's the block below the turtle and digs
		 - it if it's an ore we want.
		 -
		 - @return void
		 -]]
									down = function(self)
										if self:check("down") then
											t.digDown();
										end
									end,

									--[[
		 - Runs the block compare function based on
		 - the direction provided. This goes through
		 - all of the slots. If no direction is
		 - specified then we do a forward compare - as
		 - used by {@link left()} and {@link right()}.
		 -
		 - @param string Direction to compare
		 - @return boolean Ore block found
		 -]]
									check = function(self, dir)
										-- Slot 1 is junk, slot 2 is torches
										for i = 3, 9 do
											t.select(i);

											if dir == "up" then
												if t.compareUp() then
													return true;
												end;
											end

											if dir == "down" then
												if t.compareDown() then
													return true;
												end;
											end

											if dir == nil then
												if t.compare() then
													return true;
												end;
											end
										end

										return false;
									end,

									home = function(self)
										print("Going home!");
									end,

									start = function(self)
										self:forward();
									end,
								}

								program:setParams(arguments);
								program:loop();
							end

							--Miner({"help"}) -- {"depth=5", "sides={'left', 'right', 'up', 'down'}", "torch={'up', '6'}"});

							function depth(d)
								print(d)
							end

							function sides(s)
								print(s)
							end

							function torch(t)
								print(t)
							end

							function parse(args)
								--local depth = 64
								local sides = {}
								local torch = {}
								local pars = nil

								for k, v in ipairs(args) do
									print("parsing " .. v)
									pars = loadstring(v)()
									print(pars)
								end

								print(depth)
								--print(table.tostring(sides))
							end

							parse({"depth=5", "sides={'left', 'right', 'up', 'down'}", "torch={'up', '6'}", "pars=3"})
