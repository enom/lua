turtle = {
	dig = function() print("dig") end,
	digUp = function() print("digUp") end,
	digDown = function() print("digDown") end,
	forward = function() print("forward") end,
	back = function() print("back") end,
	up = function() print("up") end,
	down = function() print("down") end,
	turnRight = function() print("turnRight") end,
	turnLeft = function() print("turnLeft") end,
	place = function() print("place") end,
	placeUp = function() print("placeUp") end,
	placeDown = function() print("placeDown") end,
	select = function(i) print("select " .. i) end,
	drop = function() print("drop") end,
}

function Parse(...)
	--[[

	DIG - Setting X will dig and move the turtle that direction by X blocks
	x[x] = dig forward
	h[x] = dig up
	g[x] = dig down

	MOVE
	f[x] = forward
	b[x] = back
	u[x] = up
	d[x] = down

	TURN
	r = right
	l = left

	PLACE
	p = place forward
	o = place up
	i = place down

	OTHER
	sX = select slot X
	q = drop

	--]]
	if turtle == nil then
		print("Not a turtle...")

		return
	end

	local args = { ... }
	local die = false
	local buildArray = {}
	local buildKey = 1
	local buildMultiply = 0
	local buildClosed = false
	local buildFinal = ""
	local buildAt = 1
	local selectSlot = false
	local doBuildMultiply = function()
		buildMultiply = buildMultiply * 1

		if buildMultiply == 0 then
			return
		end

		if buildClosed then
			local v = table.concat(buildArray[buildKey + 1], "")

			table.insert(buildArray[buildKey], string.rep(v, buildMultiply))
			-- Remove depth key to prevent using old depth values
			table.remove(buildArray, buildKey + 1)
		else
			local n = table.getn(buildArray[buildKey])
			local v = buildArray[buildKey][n]

			-- Minus one to multiply since we already have one letter in place
			table.insert(buildArray[buildKey], string.rep(v, buildMultiply - 1))
		end

		buildMultiply = 0
		buildClosed = false
	end

	local doBuildNext = function()
		c = buildFinal:sub(buildAt, buildAt)
		buildAt = buildAt + 1

		if c == "x" then
			turtle.dig(); end

			if c == "h" then
				turtle.digUp(); end

				if c == "g" then
					turtle.digDown(); end

					if c == "f" then
						turtle.forward(); end

						if c == "b" then
							turtle.back(); end

							if c == "u" then
								turtle.up(); end

								if c == "d" then
									turtle.down(); end

									if c == "r" then
										turtle.turnRight(); end

										if c == "l" then
											turtle.turnLeft(); end

											if c == "p" then
												turtle.place(); end

												if c == "o" then
													turtle.placeUp(); end

													if c == "i" then
														turtle.placeDown(); end

														if c == "q" then
															turtle.drop(); end

															if c:find("%d") then
																turtle.select(c); end
															end

															if #args == 0 then
																print("Missing command string")

																return
															end

															-- Here we go!
															buildArray[buildKey] = {}

															-- Command builder
															for c in args[1]:gmatch(".") do
																if not c:find("%d") then
																	doBuildMultiply()
																end

																if c == "(" then
																	buildKey = buildKey + 1
																	buildArray[buildKey] = {}
																elseif c == ")" then
																	buildClosed = true
																	buildKey = buildKey - 1
																elseif c:find("s") then
																	selectSlot = true
																elseif c:find("%d") then
																	if selectSlot then
																		selectSlot = false

																		table.insert(buildArray[buildKey], c)
																	else
																		buildMultiply = buildMultiply .. c
																	end
																elseif c:find("[xhgfbudrlpoiq]") then
																	table.insert(buildArray[buildKey], c)
																else
																	print("Unknown command character: " .. c)

																end
															end

															-- Parse any multipliers left at the end of our command
															doBuildMultiply()

															-- Extracting the results
															buildFinal = table.concat(buildArray[1], "")

															print("Running...")
															print("Press any key to cancel")

															-- Command executioner
															--[[
	for c in buildFinal:gmatch(".") do
		if c == "x" then
			turtle.dig(); end

		if c == "h" then
			turtle.digUp(); end

		if c == "g" then
			turtle.digDown(); end

		if c == "f" then
			turtle.forward(); end

		if c == "b" then
			turtle.back(); end

		if c == "u" then
			turtle.up(); end

		if c == "d" then
			turtle.down(); end

		if c == "r" then
			turtle.turnRight(); end

		if c == "l" then
			turtle.turnLeft(); end

		if c == "p" then
			turtle.place(); end

		if c == "o" then
			turtle.placeUp(); end

		if c == "i" then
			turtle.placeDown(); end

		if c == "q" then
			turtle.drop(); end

		if c:find("%d") then
			turtle.select(c); end
	end
	--]]

															--[[
parallel.waitForAll(
	function()
		while not die do
			local event, key = os.pullEvent("key")
			if key ~= 1 then
				print("Stopping turtle")
				die = true
			end
		end
	end,
	function()
		while not die do
			doBuildNext()
		end
	end
)
--]]

															for i = 0, buildFinal:len() do
																doBuildNext()
															end
														end


														Parse("((xf)2s3pr)2")
