_t = turtle;
_plant = true;
_dir = "right";

--[[
 - Checks to see if the block in front is wood
 - and cuts the tree down if so.
 -]]
function _chop()
	-- Make sure we have wood selected
	_t.select(1);

	if _t.compare() then
		print("chop");

		-- Move under wood
		_t.dig();
		_t.forward();

		-- Dig up
		while _t.detectUp() do
			_t.digUp();
			_t.up();
		end

		-- Move back down
		while not _t.detectDown() do
			_t.down();
		end
	end
end

--[[
 - Main loop. Checks if we can chop down a tree
 - then makes sure we can move forward. If a dirt
 - block is found, we run the turn command.
 -]]
function _forward()
	_chop();

	print("forward");

	-- Make sure we have our area delimiter
	_t.select(2);

	if _t.compare() then
		-- Our delimiter was found so turn
		_turn();
	else
		-- Keep on with the loop
		_t.forward();

		_forward();
	end
end

--[[
 - Turns left or right depending on our previous
 - turn. This additionally checks if we hit our
 - chop area limit and makes our bot go drop its
 - inventory.
 -]]
function _turn()
	-- Make sure we have dirt selected
	_t.select(2);

	if _dir == "right" then
		print("turn right");
		_t.turnRight();

		if _t.compare() then
			-- Hit the end of our run
			_home();
		else
			-- Next turn will be to our left
			_dir = "left";

			_t.forward();
			_t.turnRight();

			-- Continue with our loop
			_forward();
		end
	else
		print("turn left");
		_t.turnLeft();

		if _t.compare() then
			-- Hit the end of our run
			_home();
		else
			-- Next turn will be to our right
			_dir = "right";

			_t.forward();
			_t.turnLeft();

			-- Continue with our loop
			_forward();
		end
	end
end

--[[
 - Returns the turtle home to drop its cargo off.
 -]]
function _home()
	print("home");
	-- Select our home identifier: disk
	_t.select(3);

	if _dir == "right" then
		-- Home is at the opposite corner
		_t.turnRight();

		-- Move along the wall to line up with home
		while not _t.detect() do
			_t.forward();
		end

		_t.turnRight();
	else
		-- Home is behind us
		_t.turnLeft();
		_t.turnLeft();
	end

	while not _t.compareUp() do
		_t.forward();
	end

	_drop();
end

--[[
 - Drops all items and resets our loop.
 -]]
function _drop()
	print("drop");

	for i = 4, 9 do
		-- Selecting our non-compare slots
		_t.select(i);

		for ii = 0, 64, 8 do
			-- Split our drops to prevent losing a block of 64
			_t.drop(ii);
		end
	end

	-- Reset
	_t.turnRight();
end

_forward();
