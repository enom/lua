-- GLOBAL VARIABLES v1.0.0 --

--[[
 - Milliseconds for loops and holds.
 -
 - @type int
 -]]
local MS = 10

-- global shortcuts
local log = OutputLogMessage
local time = GetRunningTime

-- LOOP FUNCTIONS v3.2.0 --

local l = {
	-- internal loop counter
	i = -1,
	-- oscillate value flipped
	f = false,
	-- oscillate boolean value
	o = false,
	-- resets the loop counters
	new = function(self)
		self.i = -1
		self.f = false
		self.o = false
	end,
	-- increments a value while looping
	inc = function(self)
		self.i = self.i + 1
		return self.loop()
	end,
	-- increments and oscillates a value while looping
	osc = function(self, n)
		self.i = self.i + 1
		self.f = self.i % n == 0
		if self.f then self.o = not self.o end
		return self.loop()
	end,
	-- break out of loops using SHIFT
	loop = function()
		return not IsModifierPressed("shift")
	end,
	-- stop press events using SHIFT
	stop = function()
		return IsModifierPressed("shift")
	end,
	-- break out of sleeps using SHIFT
	wait = function(ms)
		local now = time()
		local max = now + ms
		for i = now, max, MS do
			if IsModifierPressed("shift") then break end
			if time() > max then break end
			Sleep(MS)
		end
	end
}

-- KEYBOARD FUNCTIONS v3.1.1 --

local k = {
	-- press key
	p = function(btn)
		if l.stop() then return end
		PressKey(btn)
		Sleep(MS)
	end,
	-- press, hold, and release key
	pr = function(btn, hold)
		if l.stop() then return end
		PressKey(btn)
		if hold == nil then Sleep(MS) else l.wait(hold) end
		ReleaseKey(btn)
		Sleep(MS)
	end,
	-- release key
	r = function(btn)
		ReleaseKey(btn)
		Sleep(MS)
	end
}

-- MOUSE FUNCTIONS v3.3.1 --

local m = {
	-- move mouse cursor
	m = function(x, y)
		MoveMouseRelative(x, y)
	end,
	-- press mouse button
	p = function(btn)
		if l.stop() then return end
		PressMouseButton(btn)
		Sleep(MS)
	end,
	-- get/set mouse position
	pos = function(x, y)
		if not x and not y then return GetMousePosition() end
		MoveMouseTo(x, y)
	end,
	-- press, hold, and release mouse button
	pr = function(btn, hold)
		if l.stop() then return end
		PressMouseButton(btn)
		if hold == nil then Sleep(MS) else l.wait(hold) end
		ReleaseMouseButton(btn)
		Sleep(MS)
	end,
	-- release mouse button
	r = function(btn)
		ReleaseMouseButton(btn)
		Sleep(MS)
	end
}

-- BINDINGS v1.1.0 --

local M = {
	key = GetMKeyState(),
	M1 = {
		G2 = {
			--[[
			 - Clicks the teleport up/down buttons.
			 -]]
			release = function()
				local x, y = m.pos()

				m.pos(65074, 13706) -- down
				m.pr(1)
				m.pos(65074, 12706) -- up
				m.pr(1)

				m.pos(x, y)
			end
		},
		G9 = {
			--[[
			 - GAMEPAD: Clicks the take all and close buttons for different storage sizes.
			 - MOUSE: Uses a healing salve.
			 -]]
			release = function(g, mouse)
				if mouse then
					k.pr("4")
					l.wait(250)
					m.pr(1, 50)
				else
					local x, y = m.pos()
					OutputLogMessage("{%d, %d}, -".."- \n", x, y)

					local take = {
						{42589, 37208}, -- 32
						{42589, 38073}, -- 40
						{42512, 38893}, -- 48
						{42512, 39576}, -- 56
						{42691, 40624}, -- 64
						{43536, 42263}, -- 100
						{44586, 42309}, -- 120
					}

					for i = 1, #take do
						m.pos(take[i][1], take[i][2])
						m.pr(1)
					end

					local close = {
						{46097, 26733}, -- 32
						{46200, 25959}, -- 40
						{46046, 25185}, -- 48
						{46020, 24046}, -- 56
						{46123, 23272}, -- 64
						{48018, 21496}, -- 100
						{49990, 21678}, -- 120
					}

					for i = 1, #close do
						m.pos(close[i][1], close[i][2])
						m.pr(1)
					end

					m.pos(x, y)
				end
			end
		},
		G11 = {
			--[[
			 - MOUSE: Uses a bandage.
			 -]]
			release = function(g, mouse)
				if mouse then
					k.pr("4")
					l.wait(200)
					m.pr(3, 50)
				end
			end
		}
	}
}

-- DISPATCHER v3.3.0 --

function OnEvent(event, arg)
	local key = M["M"..M.key] and M["M"..M.key]["G"..arg]
	local mouse = event == "MOUSE_BUTTON_PRESSED"
		or event == "MOUSE_BUTTON_RELEASED"
	local callback = event == "G_PRESSED" and "press"
		or event == "MOUSE_BUTTON_PRESSED" and "press"
		or event == "G_RELEASED" and "release"
		or event == "MOUSE_BUTTON_RELEASED" and "release"
	if event == "M_RELEASED" then M.key = arg end
	if key and key[callback] then key[callback](key, mouse) end
end
