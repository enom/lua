-- shortened output function
p = OutputLogMessage
-- Debug alternative to disable
debug = false
d = function(...) if (debug) then OutputLogMessage(unpack({...})) end end
-- Holds all of our while loops - note these are set by running GKeys before looping
WhileLoopHolder = {}
WhileLoopParams = {
	_last = 0,

	sleep = 10, -- ms
	active = false,
	init = false,
	stop = false,

	tick = function (self)
		Sleep(self.sleep);
		SetMKeyState(1);
		self._time = GetRunningTime();
	end, -- tick()

	time = function (self)
		diff = GetRunningTime() - self._time;
		d("\t[WhileLoopParams] time: %dms", diff);

		return diff;
	end, -- time()
}

Fire = {
	key = function (k)
		PressKey(k);
		Sleep(50);
		ReleaseKey(k);
	end,

	mouse = function (k)
		PressMouseButton(k);
		Sleep(50);
		ReleaseMouseButton(k);
	end,
}



function OnEvent(event, arg)
	-- OutputLogMessage("event = %s, arg = %s\n", event, arg);

	if GR(event, arg, 10) then
		Fire.mouse(1); end

		if GR(event, arg, 12) then
			Fire.key("c"); end

			w = WhileLoopParams;

			-- Loop
			if (w.active) then
				for k, v in pairs(WhileLoopHolder) do
					-- Tick and trigger
					if MP(event, arg, 1) then
						-- Filter only actual loop calls
						v:tick(); end

						if (event == "G_PRESSED") then
							v:trigger(arg); end
						end -- foreach WhileLoops

						if MP(event, arg, 1) then
							-- Do the actual recursion
							w:tick(); end
						end -- loop

						-- Start loop prompt
						if (MP(event, arg, 1)
							and not w.active
						and not w.stop) then
							if (not w.init) then
								-- Initialize loop?
								p("\n! - ! - ! - ! - ! - ! - ! - ! - ! - ! - ! - ! - ! - ! - ! - ! - !\nAre you sure you want to recurse? [M1 for yes]\n");
								w.init = true;
							elseif (w.init) then
								-- Initialize loop!
								p("\n! - ! - ! - ! - ! - ! - ! - ! - ! - ! - ! - ! - ! - ! - ! - ! - !\nCommencing loop\n\n");
								w.init = false;
								w.active = true;
								w:tick()
							end
						end -- loop prompt

						-- Start loop cancel
						if (not MP(event, arg, 1)
							and not MR(event, arg, 1)
							and not w.stop6
						and w.init ) then
							p("Cancelling loop\n\n");
							WhileLoopParams.init = false;
						end -- cancel start

						-- Clear stop
						if (w.stop) then
							w.stop = false; end

							-- Stop loop
							if (w.active and MP(event, arg, 3)) then
								p("\n! - ! - ! - ! - ! - ! - ! - ! - ! - ! - ! - ! - ! - ! - ! - ! - !\nStopping loop\n\n");
								w.active = false;
								w.stop = true;
							end -- stop loop

							-- Create macros
							if (not w.active) then
								if GR(event, arg, 18) then
									if (WhileLoopHolder["sniper"] == nil) then
										p("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\nSniper loop set\n\n");
										sniper = WhileLoop(500)
										sniper.name = "Sniper Loop";

										sniper:action(function ()
											diff = GetRunningTime() - sniper._tick;
											p("Shot out! ~%dms > %dms \t\t[%dms tick, %dms run]\n", diff, sniper:delay(), sniper._tick, GetRunningTime());
											Fire.mouse(1);
										end);

										sniper:bind(13, function ()
											p("Adding 10ms to shot [%dms]\n", sniper:delay());
											sniper:addDelay(10);
										end);

										sniper:bind(14, function ()
											p("Removing 10ms to shot [%dms]\n", sniper:delay());
											sniper:remDelay(10);
										end);

										WhileLoopHolder["sniper"] = sniper;
									else
										WhileLoopHolder["sniper"] = nil;
										p("! - ! - ! - ! - ! - ! - ! - ! - ! - ! - ! - ! - ! - ! - ! - ! - !\nSniper loop DESTROYED\n\n");
									end
								end -- bind M1 loops
							end -- create macros
						end -- OnEvent()

						function GP(event, arg, is)
							if (event == "G_PRESSED" and arg == is) then
								return true end
								return false
							end -- GP() G_PRESSED event

							function GR(event, arg, is)
								if (event == "G_RELEASED" and arg == is) then
									return true end
									return false
								end -- GR() G_RELEASED event

								function MP(event, arg, is)
									if (event == "M_PRESSED" and arg == is) then
										return true end
										return false
									end -- MP() M_PRESSED event

									function MR(event, arg, is)
										if (event == "M_RELEASED" and arg == is) then
											return true end
											return false
										end -- MR() M_RELEASED event

										function WhileLoop(x)
											local loop = {
												_abort = false,
												_delay = 0,
												_action = nil,
												_params = nil,
												_binds = {},
												_tick = 0,

												name = "nil",

												abort = function (self, set)
													d("\t[%s] SETTING LOOP STATUS to %s\n", self.name, set);
													self._abort = set;
												end,

												stop = function (self)
													d("\t[%s] STOPPING LOOP\n", self.name);
													self.abort(true);
												end,

												reset = function (self)
													d("\t[%s] RESETTING LOOP\n", self.name);
													self.abort(false)
												end,

												delay = function (self, x)
													if (x == nil) then
														d("\t[%s] GETTING DELAY %dms\n", self.name);
														return self._delay;
													else
														d("\t[%s] SETTING DELAY to %dms\n", self.name, x);
														_delay = x;
														return self._delay;
													end -- if x nill
												end, -- delay()

												addDelay = function (self, x)
													if (x == nil) then
														x = 10; end

														self._delay = self._delay + x;
														d("\t[%s] ADD DELAY by %dms [%dms]\n", self.name, x, self._delay);
													end, -- end addDelay()

													remDelay = function (self, x)
														if (x == nil) then
															x = 10; end

															self._delay = self._delay - x;
															d("\t[%s] REMOVE DELAY by %dms [%dms]\n", self.name, x, self._delay);
														end, -- remDelay()

														action = function (self, a, p)
															d("\t[%s] ADDING ACTION\n", self.name);
															if (a == nil) then
																return; end

																self._action = a;
																self._params = p;
															end, -- action()

															tick = function (self)
																delay = round(GetRunningTime()) - self._tick;
																d("\t[%s] TICK %d; DELAY %d\n", self.name, self._tick, delay);

																if (delay >= self._delay) then -- compensate for clock offset
																	-- execute our binded action
																	self._action(self._params);

																	-- reset our internal counter
																	self._tick = round(GetRunningTime());
																end
															end, -- tick()

															bind = function (self, k, v)
																d("\t[%s] BIND %s [%s]\n", self.name, k, typeof(v));
																self._binds[k] = v;
															end, -- bind()

															trigger = function (self, k)
																d("\t[%s] TRIGGER %s [%s]\n", self.name, k, typeof(self._binds[k]));
																if (self._binds[k] ~= nil) then
																	self._binds[k](); end
																end, -- trigger()
															}

															if x == nil
															then x = 0; end

															loop._delay = x

															return loop
														end -- WhileLoop()

														function typeof(var)
															local _type = type(var);
															if(_type ~= "table" and _type ~= "userdata") then
																return _type;
															end
															local _meta = getmetatable(var);
															if(_meta ~= nil and _meta._NAME ~= nil) then
																return _meta._NAME;
															else
																return _type;
															end
														end

														function round(num, off)
															off = off or 2.5
															if (math.floor(num / off)) % 2 == 1 then
																return math.ceil((num + .01) / off) * off
															else
																return math.floor(num / off) * off
															end
														end
