abort = false;

--Explode function, works like PHP
function explode ( seperator, str )
	local pos, arr = 0, {}
	for st, sp in function() return string.find( str, seperator, pos, true ) end do -- for each divider found
		table.insert( arr, string.sub( str, pos, st - 1 ) ) -- Attach chars left of current divider
		pos = sp + 1 -- Jump past current divider
	end
	table.insert( arr, string.sub( str, pos ) ) -- Attach chars right of last divider
	return arr
end

function call(act, arg)
	action = {
		["move"] = function(arg)
			MoveMouseTo(arg[1], arg[2]);
			call("sleep", 200);
		end,
		--Used to press and release keys
		["key"] = function(arg)
			--If there are more then 1 arguments
			if (table.maxn(arg) > 1) then
				--Loop through each argument
				for i = 1, table.maxn(arg) do
					--If argument is a table
					if (type(arg[i]) == "table") then
						--Send argument table back to "key"
						call("key", arg[i]);
					else
						--Argument isn't table
						if (abort ~= true) then
							PressKey(arg[i]);
							call("sleep", 200);
						end --//if ~abort
					end --//if ~table
				end --//for arguments

				--Loop to release keys
				for i = table.maxn(arg), 1, - 1 do
					--If argument isn't table
					if (type(arg[i]) ~= "table") then
						if (abort ~= true) then
							ReleaseKey(arg[i]);
							call("sleep", 200);
						end --//if ~abort
					end --//if ~table
				end --//for release
			else
				--Only 1 argument found,
				if (abort ~= true) then
					PressAndReleaseKey(arg[1]);
					call("sleep", 200);
				end --//if ~abort
			end --//arg > 1
		end, --//"key"
		["sleep"] = function(arg)
			if (arg <= 200) then
				Sleep(arg);
			else
				for i = 1, (math.ceil(arg / 200)) do
					call("sleep", 200);
				end
			end
		end, --//"sleep"
		["chain"] = function(arg)
			--Loop through each argument
			for i = 1, table.maxn(arg) do
				--If argument is a table
				if (type(arg[i]) == "table") then
					--Send argument table back to "key"
					call("chain", arg[i]);
				else
					--Argument isn't table
					if (abort ~= true) then
						a = explode(", ", arg[i]);

						if (a[1] == "move" ) then
							MoveMouseTo(a[2], a[3])
						elseif (a[1] == "mouse") then
							PressMouseButton(a[2])
						elseif (a[1] == "key") then
							PressKey(a[2])
						elseif (a[1] == "sleep") then
							call(a[1], tonumber(a[2]));
						end --//switch case

						call("sleep", 200)
					end --//if ~abort
				end --//if table
			end --//for arguments

			--Loop to release keys
			for i = table.maxn(arg), 1, - 1 do
				--If argument isn't table
				if (type(arg[i]) ~= "table") then
					if (abort ~= true) then
						a = explode(", ", arg[i]);

						if (a[1] == "mouse") then
							ReleaseMouseButton(a[2])
						elseif (a[1] == "key") then
							ReleaseKey(a[2])
						end --//switch case

						call("sleep", 200)
					end --//if ~abort
				end
			end --//for release
		end, --//"chain"
		["say"] = function(arg)
			if (abort ~= true) then
				OutputLogMessage(arg);
			end --//if ~abort
		end, --//"say"
	};

	if (IsModifierPressed("lshift") ~= true and abort ~= true) then
		action[act](arg);
	elseif (IsModifierPressed("lshift") == true and abort ~= true) then
		OutputLogMessage("Aborting macro.\n");
		abort = true;
		runs = 0;
	end

end


function OnEvent(event, arg)

	----------------------------------------------------------------------------
	--                               SETTINGS                                 --
	----------------------------------------------------------------------------

	--Drone tab
	dx, dy = 51913, 45231;
	--Roid bookmark
	rx, ry = 32123, 25802;
	--First target in overview
	tx, ty = 51874, 13994;
	--Cargo hold tab
	cx, cy = 7260, 20804;
	--Items tab
	ix, iy = 30445, 42045;
	--Undock button
	ux, uy = 5386, 58288;
	--Station bookmark
	sx, sy = 35012, 23365;
	--Drop location
	hx, hy = 27088, 24490;

	runs = 0;
	targets = 3;
	targetTime = 7.5 * 1000;
	cycleTime = 3 * 60 * 1000;
	recallTime = 15 * 1000;
	dockTime = 30 * 1000;
	warpTime = 1.75 * 60 * 1000;
	sTime = "";
	eTime = "";

	if (event == "G_PRESSED" and arg == 1) then
		OutputLogMessage("Reseting macro.\n");
		abort = false;
	end

	if (event == "G_PRESSED" and arg == 16) then
		call("say", "Doing auto-sell");


		while not abort do
			call("chain", {
				{"move, " .. 1366 .. ", " .. 3705},
				{"mouse, 1"},
				{"move, " .. 8913 .. ", " .. 23019},
				{"mouse, 1"}
			});

			call("sleep", 250);
		end -- while
	end -- G16

	if (event == "G_PRESSED" and arg == 17) then
		while not abort do
			call("say", "Pew! ");
			PressMouseButton(1);
			call("sleep", 50);
			ReleaseMouseButton(1);
			call("sleep", 615);
		end -- while
	end -- G17

	if (event == "G_PRESSED" and arg == 18) then
		x, y = GetMousePosition();
		OutputLogMessage(x .. ", " .. y .. "\n");
	end

	----------------------------------------------------------------------------
	--                      TRI BOX - MINER/MINER/HAULER                      --
	----------------------------------------------------------------------------
	--WORK IN PROGRESSv
	if (event == "G_PRESSED" and arg == 11) then
		while not abort do
			call("say", "Unloading cargo.\n");
			call("chain", {
				{"move, "}
			});
		end -- while
	end -- G11

	----------------------------------------------------------------------------
	--                         SINGLE BOX - ORE DROP                          --
	----------------------------------------------------------------------------

	if (event == "G_PRESSED" and arg == 2) then
		while not abort do
			call("say", "Unloading cargo.\n");

			call("chain", {
				{"move, " .. cx .. ", " .. cy},
				{"mouse, 1"},
				{"key, lctrl",
					{"key, a"}
				},
				{"mouse, 1",
					{"move, " .. hx .. ", " .. hy}
				}
			});

			call("sleep", cycleTime);
		end
	end

	----------------------------------------------------------------------------
	--                         DUAL BOX - MINERS DUO                          --
	----------------------------------------------------------------------------

	if (event == "G_PRESSED" and arg == 13) then
		while not abort do
			call("say", "Unloading cargo.\n");

			for i = 0, 1 do
				call("chain", {
					{"move, " .. cx .. ", " .. cy},
					{"mouse, 1"},
					{"key, lctrl",
						{"key, a"}
					},
					{"mouse, 1",
						{"move, " .. hx .. ", " .. hy}
					},
					{"key, lalt",
						{"key, tab"}
					}
				});
			end

			call("sleep", cycleTime);
		end
	end

	----------------------------------------------------------------------------
	--                         SINGLE MINING MACRO BOT                        --
	----------------------------------------------------------------------------


	if (event == "G_PRESSED" and arg == 10) then


		while not abort do

			call("say", "Activating mining macro.\n");

			----------------------------------------------------------------
			--      Launch drones


			call("say", "Releasing drones.\n");
			call("chain", {
				{"move, " .. dx .. ", " .. dy},
				{"mouse, 3"},
				{"move, " .. (dx + 2576) .. ", " .. (dy + 1749)},
				{"mouse, 1"},
			});

			----------------------------------------------------------------
			--      Manage bookmarks

			--Remove bookmark
			call("say", "Removing old bookmark.\n");
			call("chain", {
				{"move, " .. rx .. ", " .. ry},
				{"mouse, 3"},
				{"move, " .. (rx + 4060) .. ", " .. (ry + 7435)},
				{"mouse, 1"}
			});

			--Create new bookmark
			call("say", "Creating new bookmark.\n");
			call("chain", {
				{"move, " .. tx .. ", " .. ty},
				{"mouse, 3"},
				{"move, " .. (tx + 3905) .. ", " .. (ty + 6872)},
				{"mouse, 1"},
				{"sleep, 200"},
				{"key, enter"}
			});

			----------------------------------------------------------------
			--      Target asteroids

			for i = 0, (targets - 1) do
				--Calculate new position for target n
				yOff = ty + (i * 1187);

				--Target
				call("say", "Targeting roid " .. (i + 1) .. ".\n");
				call("chain", {
					{"move, " .. tx .. ", " .. yOff},
					{"key, lctrl",
						{"mouse, 1"}
					}
				});
			end -- //

			--Wait till targeted (10 seconds)
			call("sleep", targetTime);

			----------------------------------------------------------------
			--      Activate strip miners

			for i = 0, (targets - 1) do
				--Determines which laser to activate
				fKey = "f" .. (i + 1);
				yOff = ty + (i * 1187);

				--Activate
				call("say", "Stripping roid: " .. fKey .. ".\n");
				call("chain", {
					{"move, " .. tx .. ", " .. yOff},
					{"mouse, 1"},
					{"key, " .. fKey}
				});
			end -- //

			--Wait till one cycle from strip miners has been completed
			call("sleep", cycleTime)

			----------------------------------------------------------------
			--      Return drones

			call("say", "Returning drones.\n");
			call("chain", {
				{"move, " .. dx .. ", " .. (dy + 1187)},
				{"mouse, 3"},
				{"move, " .. (dx + 2576) .. ", " .. (dy + 3561 + 1187)},
				{"mouse, 1"}
			});


			call("sleep", recallTime);

			----------------------------------------------------------------
			--      Request docking

			for i = 0, 2 do
				--Request docking twice, sometimes first attempt fails
				call("say", "Docking attempt number " .. (i + 1) .. ".\n");
				call("chain", {
					{"move, " .. sx .. ", " .. sy},
					{"mouse, 3"},
					{"move, " .. (sx + 2927) .. ", " .. (sy + 2561)},
					{"mouse, 1"},
					{"key, lctrl",
						{"key, w"}
					}
				});

				--Wait till docked
				if (i == 0) then
					call("sleep", warpTime + dockTime);
				else
					call("sleep", dockTime);
				end
			end -- //


			----------------------------------------------------------------
			--Drop cargo into items in station--

			call("say", "Unloading cargo.\n");
			call("chain", {
				{"move, " .. cx .. ", " .. cy},
				{"mouse, 1"},
				{"key, lctrl",
					{"key, a"}
				},
				{"mouse, 1",
					{"move, " .. ix .. ", " .. iy}
				},
				{"sleep, 200"}
			});

			----------------------------------------------------------------
			--Undock from station--

			call("say", "Undocking from station.\n");
			call("chain", {
				{"move, " .. ux .. ", " .. uy},
				{"mouse, 1"}
			});
			call("sleep", dockTime);

			----------------------------------------------------------------
			--Warp to 0 on bookmarked location--

			call("say", "Warping to bookmark.\n");
			call("chain", {
				{"move, " .. rx .. ", " .. ry},
				{"mouse, 3"},
				{"move, " .. (rx + 3982) .. ", " .. (ry + 687)},
				{"mouse, 1"},
			});

			--Wait until arrived at destination
			call("sleep", warpTime);

			if (abort ~= true) then
				runs = runs + 1;
				OutputLogMessage("Run completed. Current: " .. runs .. ".\n");
			else
				OutputLogMessage("Mining operation aborted. Runs: " .. runs .. ".\n");
			end
		end -- while
	end

	----------------------------------------------------------------------------
	--                            GET MOUSE POSITION                          --
	----------------------------------------------------------------------------

	if (event == "G_PRESSED" and arg == 12) then
		x, y = GetMousePosition();
		OutputLogMessage("Mouse is at %d, %d\n", x, y);
	end
end
