-- require g-key-vars@1.0.1
-- require g-key-loop@3.2.0

-------------------------------
-- KEYBOARD FUNCTIONS v3.3.1 --
-------------------------------

local K = {
	--  press or release the CTRL key
	ctrl = function(press)
		if press then
			PressKey("rctrl")
		else
			ReleaseKey("rctrl")
		end
	end,
	-- press and release the Enter key
	enter = function()
		PressKey("enter")
		Sleep(MS)
		ReleaseKey("enter")
		Sleep(MS)
	end,
	-- press key
	p = function(btn)
		if L.stop() then return end

		PressKey(btn)
		Sleep(MS)
	end,
	-- press, hold, and release key
	pr = function(btn, hold)
		if L.stop() then return end

		PressKey(btn)

		if hold == nil then Sleep(MS) else L.wait(hold) end

		ReleaseKey(btn)
		Sleep(MS)
	end,
	-- release key
	r = function(btn)
		ReleaseKey(btn)
		Sleep(MS)
	end,
	-- press or release the SHIFT key
	shift = function(press)
		if press then
			PressKey("rshift")
		else
			ReleaseKey("rshift")
		end
	end,
	-- type out a message
	type = function(message)
		local r = {
			["-"] = "minus",
			[";"] = "semicolon",
			["/"] = "slash",
			[" "] = "spacebar",
			["="] = "equal"
		}

		local u = {
			["~"] = "tilde",
			["!"] = "1",
			["@"] = "2",
			["#"] = "3",
			["$"] = "4",
			["%"] = "5",
			["^"] = "6",
			["&"] = "7",
			["*"] = "8",
			["("] = "9",
			[")"] = "0",
			["_"] = "minus",
			["+"] = "equal"
		}

		for c in message:gmatch(".") do
			local code = u[c] or r[c] or c
			local shift = u[c] or c:match("%u")

			if L.stop() then return end
			if shift then PressKey("rshift") end

			PressKey(code)
			Sleep(MS)
			ReleaseKey(code)
			Sleep(MS)

			if shift then ReleaseKey("rshift") end
		end
	end
}
