
------------------------------
-- GAMEPAD FUNCTIONS v1.1.0 --
------------------------------

local G = {
	-- changes gamepad background color
	bg = function(r, g, b)
		if type(r) == "table" then r, g, b = unpack(r) end

		SetBacklightColor(r, g, b, "lhc")
	end,
	-- properly spaces a message to center it on the gamepad
	center = function(message)
		return "\n" .. string.rep(" ", (50 - (#message * 1.618)) / 2) .. message .. "\n"
	end,
	-- clear gamepad color and text
	clear = function(color)
		if color ~= false then SetBacklightColor(255, 255, 255, "lhc") end

		OutputLCDMessage("", 0)
		ClearLCD()
	end,
	-- output a message to the gamepad
	out = function(message, timeout)
		OutputLCDMessage(message, timeout or 1000 * 60)
	end,
	-- colorize the output of a message to the gamepad
	text = function(self, color, ...)
		self.bg(color)
		self.out(...)
	end,
	-- coloured text
	black = function(self, ...)
		self:text({0, 0, 0}, ...)
	end,
	white = function(self, ...)
		self:text({255, 255, 255}, ...)
	end,
	grey = function(self, ...)
		self:text({127, 127, 127}, ...)
	end,
	red = function(self, ...)
		self:text({255, 0, 0}, ...)
	end,
	orange = function(self, ...)
		self:text({255, 127, 0}, ...)
	end,
	yellow = function(self, ...)
		self:text({255, 255, 0}, ...)
	end,
	green = function(self, ...)
		self:text({0, 255, 0}, ...)
	end,
	teal = function(self, ...)
		self:text({0, 255, 127}, ...)
	end,
	aqua = function(self, ...)
		self:text({0, 255, 255}, ...)
	end,
	blue = function(self, ...)
		self:text({0, 0, 255}, ...)
	end,
	purple = function(self, ...)
		self:text({127, 0, 255}, ...)
	end,
	fushia = function(self, ...)
		self:text({255, 0, 255}, ...)
	end
}
